TRS-80 Model I, III and 4 Disassembly and Reverse-Engineering
=============================================================

Directories:
- `doc/`: Documentation native to this repository, generally in Markdown
  (`.md`) files. These also include links to further information on the web.

Selected Files:
- [`doc/common.md`](doc/common.md): Hardware information commmon to several
  or all of the Model I/III/4/4P.
- [`doc/m1.md`](doc/m1.md): Model I Hardware
- [`doc/emu.md`](doc/emu.md): Information on emulators.

### References

- F.J. Kraan's pages for the [Model I][kraan1], and [Model 4P][kraan4]
  contain a wealth of information, disassemblies, disk images, projects and
  so on. (He also a considerable number of pages on other computers.)



<!-------------------------------------------------------------------->
<!-- References -->
[kraan1]: https://electrickery.nl/comp/trs80
[kraan4p]: https://electrickery.nl/comp/trs80-4p
