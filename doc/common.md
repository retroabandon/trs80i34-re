TRS-80 Model I/III/4/4P Common Hardware Notes
=============================================

Other Documentation Files:
- [Video Subystem](./video.md)
- [Model I Hardware](./m1.md)

### References

General References:
- TRS-80 Revived Site, [Ports and I/O Devices][tr-pio].
  Model I, Model III, Model 4, Model 4P. Includes networking etc.


UART/RS-232
-----------

The UART is a TR1865, TR1602, Intersil [HD-6402], or equivalent.

I/O Ports:

    EB rw   UART data register
    EA ww   UART control/status
    E9 r    settings switches sense (Model I only)
            b7    parity  0=odd  1=even
            b5-6  word length  00=5  01=6  10=7  11=8
            b4    parity  0=enabled  1=disabled
            b3    unused?
            b0-2  baud rate select (300/1200/???)
    E9  w   baud rate select
    E8 rw   modem status

See also:
- hytherion.com, [TRS-80 Model 4/4P I/O Interfacing][hyth]



<!-------------------------------------------------------------------->
[tr-pio]: https://www.trs-80.com/wordpress/ports-and-i-o-devices/

<!-- UART/RS-232 -->
[HD-6402]: https://web.archive.org/web/20050128113936/www.intersil.com/data/fn/fn2956.pdf
[hyth]: http://hytherion.com/beattidp/comput/mod4-io.htm
