TRS-80 Model I/III/4/4P Emulators
=================================

### xtrs

__xtrs__ is Tim Mann's TRS-80 Model I/III/4/4P emulator for X11 on Unix. A
Debian package, `xtrs` is available in Debian-derived Linux systems. There
is also a port of this for Linux, Windows and MacOS X, [SDLTRS].

Documentation and further info can be found at:
- [Tim Mann's TRS-80 Pages][xtrs-tm]. This includes many links to
  additional resources, such as Misosys software and documents, Catweasel
  floppy read/write tools, and so on.
- [xtrs: TRS-80 Model I/III/4/4P Emulator for Unix][xtrs-home].
- GitHub project [`TimothyPMann/xtrs`][xtrs-gh].
- `/usr/share/doc/xtrs/` when the Debian `xtrs` package is installed.

There is also a port of this, __SDLTRS,__ for Linux, Windows and MacOS,
though there seem several different versions:
- [Sourceforge project home page][sdltrs-hp], [docs][sdltrs-sf].
- [Current GitHub repo][sdltrs-gh]



<!-------------------------------------------------------------------->

<!-- xtrs -->
[sdltrs-gh]: https://gitlab.com/jengun/sdltrs
[sdltrs-hp]: https://sdltrs.sourceforge.net/
[sdltrs-sf]: https://sdltrs.sourceforge.net/docs/
[xtrs-gh]: https://github.com/TimothyPMann/xtrs
[xtrs-home]: https://www.tim-mann.org/xtrs.html
[xtrs-tm]: http://www.tim-mann.org/trs80.html
