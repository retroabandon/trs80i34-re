Model I Hardware
================

Other Documentation Files:
- [Common Hardware Notes](./common.md)
- [Video Subystem](./video.md)

#### References

- TRS-80 Home Page, [Model 1 Internals][thp-m1i]
- GitHub [RetroStack][rst-home]. Contains many projects, mostly Model I
  related. [TRS-80-Model-I][rst-m1] has an overview of some of them.
  Individual ROM and replica projects are listed below.

ROMs:
- RetroStack, [Character_Generator_ROMs][rst-cgrom]. Dumps of various
  Model I chargen ROMs including original Tandys, clones, custom charsets
  and a few non-TRS computers. Includes PNG images showing charsets.

Mainboard replicas:
- RetroStack, [TRS-80-Model-I-G-E1][rst-m1g]. Exact replica of Model I
  rev G motherboard.
- Board-Folk, [TRS80IUS]. Replica of final 'G' version of American
  motherboard. Additional headers added to allow use of 256-code charset.
  KiCad source, BOM, Gerbers.
- Board-Folk, [TRS80IJP]. Replica of Japanese (TEC) motherboard. Includes
  second header to allow use of US keyboard instead of JP keyboard. Quite
  different from the `26-7016` and `26-01004-G` models listed below. KiCad
  source, BOM, Gerbers.

Other parts replicas:
- RetroStack, [MCM776x_CharGen_Adapter][rst-cgemu]. Hardware to replace
  MCM7760, MCM7764 etc. character generator ROM chips used in many
  different computers with a standard 27C256 EPROM.
- RetroStack, [TRS-80-Model-I-Keyboard-ALPS][rst-kb]. Replica of US
  keyboard. (Does not include expansion ROM board.)
- RetroStack, [TRS-80-Model-I-Power-Supply][rst-psu]. Replica, including 3D
  printed case. Transformer currently available from Mouser.
- RetroStack, [TRS-80-Model-I-Parts][rst-parts]. Various small parts,
  including cover panels, clips and stand-offs. (But PCB stand-offs can
  just be replaced with rubber tubing.)


Model Numbers
-------------

- `26-01004-G`: Another Japanese model?
- `26-7016`: Japanese model. 170069G mainboard.
  - Different keyboard layout with kana. Keyboard PCB and expansion ROM PCB
    merged into one PCB.
  - Kana character ROM `8046671-7934`.
  - JP ROM, variant of 1.2 but with extra 2K ROM added.


Power Supplies
--------------

The Model I requires:
- +5 V, ~ 1.2 A: general system use, via on-board half-wave rectifier and
  723 linear regulator.
- +12 V, ~350 mA: for DRAM, via on-board (separate) 723 linear regulator.
- -5 V, ~1 mA: for DRAM, regulated by a zener diode.

Note that the +12 V supply must be working properly for the +5 V supply to
work.

The external supplies connect with a DIN-5 connector and provide 17-21.1
VAC at 1-1.5 A on pins 1/3 (for +5 V and -5 V) and unregulated 16-20 VDC at
0.35 A on pins 4=- and 2=+ (for +12 V). There are [several different
models][sr psu]:
- 4000004, US: 120V 60Hz 50W → 17.0 VAC 1A, 19.8 VDC .35A
- 4000007, US: 120V 60Hz 50W → 17.0 VAC 1A, 19.8 VDC .35A
- 26-9100, Europe: 240V 50Hz 50VA → 21.2 VAC 1A, 17.6 VDC .35A
- 26-9100, Europe: 220-240V 50Hz 50VA → 21.2 VAC 1A, 17.6 VDC .35A
- 26-9100, Japan: 100V 50-60Hz 32VA → 19.2 VAC 1A, 16.2 VDC .35A
- 26-9100, Australia: 240V 50Hz → 15 VAC 1.4A, 20 VDC .35A

#### References

- TRS080 Micro Computer Techncical Reference Handbook, [System Power
  Supply][trh p52].
- Shock Risk, [TRS-80 Model 1 Power Supply][sr psu].


Expansion Interface
-------------------

There were a couple of different wiring variations for the Model I
expansion port and expansion interface, including a line that was changed
from Vcc (providing minimal current) to GND, and different cables as well.
Some VCF Forum threads, including [this one][vcf 1243315], provide more
info.


CMT (Cassette Tape) I/O
-----------------------

The CMT is 500 baud FM (alternating clock and data present/absent pulses).

### XRX Cassette Modification

Some TRS-80 Model I systems have an [XRX mod][t8o-xrx], an extra board
implementing a fix for CMT loading in Level II ROM versions < 1.3 that made
it much less sensitive to the volume level of the CMT deck.
- Original version introduced in early 1979 as free upgrade at shops.
  (RS Service Information Bulletin 1130.)
- From then on factory installed in new systems until ROM 1.3 released.
  Indicated with `-1` appended to the catalog number (e.g., `26-1004-1`
  instead of `26-1004`).
- XRX-III version introduced in late 1979; supposed to fix incompatibility
  with ROM 1.3.
- Not recommended for ROM 1.3 systems, and not factory installed in final
  late-1980 ROM 1.3 systems.
- Never needed for Model III (or CoCo) systems.

The issue was that reading was done by reading a clock plus, delaying a
short time, then reading presence/absence of a data pulse. Pre-1.3 ROMs
started listening for the data pulse immediately after the clock plus,
rather than after the delay; too low a volume level would miss the clock
pulse but too high a level read noise during the delay as a data pulse.

The mod disconnects the input op amps from the main board CMT data input
flip-flop for a short period of time when that flip-flop is reset. This
helps the early avoid the ROM code, which starts listening too early, from
interpreting noise as data.

The mod is a trace cut (Z4 p10 to Z24 p9) and a small board with a 4040
12-bit counter (DIP-16), 4001 quad NOR gate (DIP-14), two diodes and a
resistor. More details and a replica of it can be found in the RetroStack
[TRS-80-Model-I-XRX-III][rst-cmt] repo.

References:
- Matthew Reed, TRS-80.org blog entry, [The XRX Cassette Modification][t8o-xrx].
- RetroStack, [TRS-80-Model-I-XRX-III][rst-cmt]. Hardware replica.


Memory Map
----------

### I/O Ports

    === CMT/Video Control
    FF r    b7    CMT input 0=low, 1=high
            b6-0  unused
    FF  w   b4-7  unused
            b3    64/32 char mode
            b2    CMT motor relay
            b0-1  CMT out voltage level  00=.85V  10=0V  01=.46V



<!-------------------------------------------------------------------->
[TRS80IJP]: https://github.com/Board-Folk/TRS80IJP
[TRS80IUS]: https://github.com/Board-Folk/TRS80IUS
[rst-cgemu]: https://github.com/RetroStack/MCM776x_CharGen_Adapter
[rst-cgrom]: https://github.com/RetroStack/Character_Generator_ROMs
[rst-home]: https://github.com/RetroStack/
[rst-kb]: https://github.com/RetroStack/TRS-80-Model-I-Keyboard-ALPS
[rst-m1]: https://github.com/RetroStack/TRS-80-Model-I
[rst-m1g]: https://github.com/RetroStack/TRS-80-Model-I-G-E1
[rst-parts]: https://github.com/RetroStack/TRS-80-Model-I-Parts
[rst-psu]: https://github.com/RetroStack/TRS-80-Model-I-Power-Supply
[thp-m1i]: http://cpmarchives.classiccmp.org/trs80/mirrors/kjsl/www.kjsl.com/trs80/mod1intern.html

<!-- Power Supplies -->
[trh p52]: https://archive.org/details/TRS-80_Micro_Computer_Technical_Reference_Handbook_2nd_1982_Radio_Shack/page/n52/mode/1up?view=theater
[sr psu]: https://www.shockrisk.com/index.php/2023/10/14/trs-80-model-1-power-supply/

<!-- Expansion Interface -->
[vcf 1243315]: https://forum.vcfed.org/index.php?threads/trs-80-model-1-wont-power-when-attached-to-expansion-interface.1243315/

<!-- CMT (Cassette Tape) I/O -->
[rst-cmt]: https://github.com/RetroStack/TRS-80-Model-I-XRX-III
[t8o-xrx]: http://www.trs-80.org/xrx-modification/
