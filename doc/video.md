TRS-80 Video Subsystem
======================

The video subsystem uses an MCM6670-style character generator ROM, which
is not mapped into the CPU address space. For more details on that ROM,
and dumps, see [`rom/chargen/`](../rom/chargen/).
