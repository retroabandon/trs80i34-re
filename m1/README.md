TRS-80 Model I Stuff
====================

- `arcdaudo.bas` is an "Arcade Audio" program by Don Dilley (from CLOAD,
  1980). It's a template showing you how to add arcade sounds to BASIC
  programs. [Source][arcdaudo].



<!-------------------------------------------------------------------->
[arcdaudo]: https://willus.com/trs80/?-a+1+-p+552+-f+1
