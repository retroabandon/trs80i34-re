TRS-80 ROM Images
=================

For character set ROM information and images, see the
[`charset/`](charset/) subdir.

#### TRS-80 Model I, Level I BASIC

__References:__
- \[andyv] GitHub `andyv/level1`, [Annotated Disassembly of the TRS-80
  Level 1 ROM][andyv].
- \[48k.ca] [Basic Disassembly][48k.ca]. A disassembly parameterized for
  `model = 1` or `model = 3` to produce the very similar Model I and
  Model III Level I BASIC ROMs.
- \[altair4k32] [Altair BASIC 3.2 (4K) - Annotated
  Disassembly][altair4k32]. HTML pages with an annotated disassembly.
  Single-file version in GitHub project [`option8/Altair-BASIC`].

#### TRS-80 Model I, Level II BASIC

__Files:__
- `M1L1.bin`: Model I Level I ROM [[wowroms]]. There appear to have been
  two slightly different version of the Level I ROMs (checksums $5A51 and
  $9F9A, or checksum $5D0C and $99C2); not sure which one this is.
- `M1L2_1.2.bin`: Model I Level II V1.2 ROM. The most common one. Differs
  from 1.1 only in the five unused bytes at the end of the ROM. [[s80]]
- `M1L2_1.3.bin`: Model I Level II V1.3 ROM. Final revision, sometimes
  called "R/S L2 BASIC" for the shortened startup banner on non-disk boot.
  [[s80]]
- `M1L2_JP.bin`: Japanese Model I ROM. This is 14K instead of 12K (three
  2332 ROMs as the original, plus an additional 2316 with only 1K of data
  written), and is based on the American V1.2 ROM. Note that the Japanese
  system also has a different keyboard PCB that includes the ROM sockets
  and decoding (the US versions have a separate ROM daughterboard), and
  a slightly different keyboard layout (and keycaps).

__References:__
- \[t8c-cs] [ROM Comparison Page (Model I)][t8c-cs]. Level I, II and clone
  versions, checksums, low-level details of the differences between ROMs,
  how to get CRCs and version numbers from BASIC.
- Matthew Reed's blog, [Model I Level II BASIC ROM Versions][reed1.II].
  List of standard ROM versions and their differences.

#### TRS-80 Model 4

- `m4p_1.16.asm`: Commented z80dasm output from Adam Rubin, downloaded from
  F.J. Kraan's [TRS-80 4P][eltr] page. (That page also contains other good
  information.)


General Sources and References
------------------------------

__ROM Images and information:__
- \[rominfo] [TRS-80 ROM Information][rominfo] on Ira Goldklang's TRS-80
  Revived Site. This does not contain any of the ROMs themeselves, but does
  have extensive information about the various versions of ROMs (including
  clones) and their APIs.
- \[wowroms] [TRS-80 MODEL I][wowroms-M1L1]. Level I ROM.
- \[s80] [A Tribute to the Dick Smith System 80][s80]. The Dick Smith
  System 80 was an Australisan semi-clone of the TRS-80 Model I. The ROM
  images are found on [this link to a ZIP file][s80zip] that's not on that
  page itself.
- \[apuder] [`TRS-80/var/rom`][apuder]. ROMs for Models I, II, III, 4 and
  4P; not clear which versions they are. (The repo itself is a TRS-80
  Emulator for Android.)

Disassemblies and API information (General)
- \[farv81] James Farvour, [_Microsoft BASIC Decoded & Other
  Mysteries_][farv81]. Explanation and disassembly of TRS-80 Level II
  BASIC.
- VCF Forum post [TRS-80 Level II BASIC cross-assembly source?][vcf4] has
  links to various disassemblies and other information. A [later
  post][vcf8] has a an attached ZIP file `levelII.zip` containing an
  annotated disassembly of an unspecified version of the Level II ROM.



<!-------------------------------------------------------------------->

<!-- Model I Level I -->
[andyv]: https://github.com/andyv/level1
[48k.ca]: http://48k.ca/L1Basic.html
[altair4k32]: http://altairbasic.org/
[`option8/Altair-BASIC`]: https://github.com/option8/Altair-BASIC

<!-- Model I Level II -->
[t8c-cs]: https://www.trs-80.com/wordpress/roms/checksums-mod-1/
[reed1.II]: http://www.trs-80.org/model-1-level-2-basic-rom-versions/

<!-- ROMs, Sources, References -->
[apuder]: https://github.com/apuder/TRS-80/tree/master/var/rom
[eltr]: https://electrickery.nl/comp/trs80-4p/
[rominfo]: https://www.trs-80.com/wordpress/roms/
[s80]: https://www.classic-computers.org.nz/system-80/
[s80zip]: https://www.classic-computers.org.nz/system-80/s80-roms.zip
[wowroms-M1L1]: https://wowroms.com/en/roms/mame/trs-80-model-i/109331.html

<!-- Disassemblies and API information -->
[farv81]: https://archive.org/details/microsoftbasicde0000farv/
[vcf4]: https://forum.vcfed.org/index.php?threads/trs-80-level-ii-basic-cross-assembly-source.1246043/post-1355586
[vcf8]: https://forum.vcfed.org/index.php?threads/trs-80-level-ii-basic-cross-assembly-source.1246043/post-1355666
