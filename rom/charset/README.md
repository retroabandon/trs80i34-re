TRS-80 Character Set ROMs
=========================

### Files

- `8046671-7934.bin`: JP charset dumped from a Model I 26-7016 with a
  Backbit Chip Tester Pro V2. Note that this appears to be the correct
  version, as compared to the katakana ROM #17 from [[cgr]].

### References

- Wikipedia, [TRS-80 character set][wcs].
- \[cgr] RetroStack, [Character_Generator_ROMs][cgr]. Contains TRS-80
  Model I ROM images and charset pictures, including many custom designs.
  However, the alleged original ROM dumps are not always reliable, e.g.,
  the katakana one (#17) appears to have the correct PNG image, but the
  dump itself does not match that.


Character ROMs
==============

The character generator ROMs are not mapped into the CPU's address space,
but are accessible only to the video circuitry. They are Tandy custom
variants of the Motorola [MCM6670/MCM6674] 128c X 7 X 5 character generator
(DIP-18N). This is a fairly ordinary ROM except that it identifies the
character on address lines A0-A6, the row on low-order address lines
RS1-RS3, and outputs the 5 bits of the row on D0-D4.

### Model III/4

Model III and later had two general types of character set ROM: original
for US and Japan, with katakana, and international for Europe, replacing
kana and a few symbols with more accented characters. [vcf 1376446]

To see which you have:

> Type `?chr$(21)chr$(22)chr$(255)`. An accented A means you have the
> international version. If you see a spaceship than `?chr$(22)` to have it
> switch. If you see nothing then try again with `?chr$(21)chr$(255)`.

(Character 22/$16 toggles katakana on the Model 3. Characters 21/$15 isn't
mentioned as doing anything on the [Wikipedia page][wcs].)

__Model III__

8044316 is the original ROM (with katakana), 8044316A (released 1981-1H) is
an update that revises the katakana glyphs. [vcf 1377433]

European charset ROM 8049000 was used in Belgian systems in 1982.

__Model 4/4P__

Early model 4s used the Model III 3044316A. But by 1983-12 they had
switched to the 8049007, which included more European characters from the
8049000. [vcf 1377433]

The 8049007, the only character generator designed for the Model 4, has an
'Ä' in the zero position, while all the Model III generators wasted that
spot on a redundant space character; the Model III hardware more or less
required a blank be there.

> Recall that on the Model I, when the CPU and the video redisplay hardware
> tried to access video RAM simultaneously, the CPU won and a glitch in the
> redisplay would occur, with one line of a character cell glitching to
> black. This is lovingly and accurately recreated in George's emulator. On
> a Model III, this problem was much reduced, but persisted in the leftmost
> columns of the screen. The thing is, on a Model III, a glitched line of a
> character doesn't exactly glitch to black. What happens is that when the
> redisplay hardware tries to read a character from video RAM and loses to
> a competing read by the CPU, the byte vale zero is returned, and then a
> line of character zero is drawn. Thus, if you use a chargen that doesn't
> have a blank in character zero, then instead of lines glitching to black,
> they'll glitch to lines of character zero, with some white pixels. This
> would look much worse than the glitching to black. On a Model 4, the
> glitching was eliminated, and the whole 8-bit character set could be
> filled. [vcf 1377433]



<!-------------------------------------------------------------------->
[cgr]: https://github.com/RetroStack/Character_Generator_ROMs/
[wcs]: https://en.wikipedia.org/wiki/TRS-80_character_set

[MCM6670/MCM6674]: ../../datasheet/MCM6670_chargen.pdf
[vcf 1376446]: https://forum.vcfed.org/index.php?threads/trs-80-model-4-character-map.1247517/post-1376446
[vcf 1377433]: https://forum.vcfed.org/index.php?threads/trs-80-model-4-character-map.1247517/post-1377433
