; z80dasm 1.1.0
; command line: z80dasm -l -t -z -g 00 bootRom.bin

	org	00000h

l0000h:
	di			;0000	f3 	  ;Start here when Reset switch is pressed
	ld b,(hl)		;0001	46 	F ;Four pointless LD instructions which...
	ld b,h			;0002	44 	D ;spell the initials of the ROM's author.
	ld c,c			;0003	49 	I ;When using BOOT command in 5.x & 6.x..
	ld d,(hl)		;0004	56 	V ;execution of ROM will start at 0005H.
l0005h:
	jr l003eh		;0005	18 37 	  ;Skip over the RSTs, and continue below
	nop			;0007	00 	  ;Next are RST vectors -- all jump to RAM
	jp 04000h		;0008	c3 00 40  ;RST 8 (Disk I/O) -- will point to a copy of
    
l000bh:
	jp l08b5h		;000b	c3 b5 08  ;<==this instruction, so a RST 8 will go...
    
	nop			;000e	00 		;from ROM to RAM and back to ROM.
	nop			;000f	00 		;
l0010h:
	jp 04003h		;0010	c3 03 40 	;RST 10H (Display String) will also point...
    
	jp l0249h		;0013	c3 49 02 	;to a copy of the instruction on the...
    
	nop			;0016	00 		;following line.
	nop			;0017	00 		;
	jp 04006h		;0018	c3 06 40 	;RST 18H (Display Message)
    
	jp l0255h		;001b	c3 55 02 	;Ditto
    
l001eh:
	nop			;001e	00 		;
	nop			;001f	00 		; 
l0020h:
	jp 04009h		;0020	c3 09 40 	;RST 20H (Get Byte)
    
	jp l0b74h		;0023	c3 74 0b 	;Initialized to get byte from MODEL%/III file 
    
	nop			;0026	00 		;(Vector in RAM is altered for RS-232 boot)
	nop			;0027	00 		;
	jp 0400ch		;0028	c3 0c 40 	
    
	jp l0b48h		;002b	c3 48 0b 	;RST 28H (Load Object Code)
    
	nop			;002e	00 	;
	nop			;002f	00 	;
l0030h:
	jp 0400fh		;0030	c3 0f 40 	;RST 30H (Scan Keyboard)
    
l0033h:
	jp l083eh		;0033	c3 3e 08 	;Ditto 
    
	nop			;0036	00 		;
	nop			;0037	00 		;
	jp 04012h		;0038	c3 12 40 	;RST 38H -- Mode 1 Interrupts
    
	ret			;003b	c9 		;If any interrupts, just ignore them
	nop			;003c	00 		;(These RSTs jump to the same addresses...
	nop			;003d	00 		;as the RSTs in the Model III ROM.)
l003eh:
	xor a			;003e	af 		;Initialization continues here
	out (0e4h),a		;003f	d3 e4 		;NMI mask -- no NMIs, please
	ld a,050h		;0041	3e 50 		;Use 4 MHz clock, enable I/O bus
	out (0ech),a		;0043	d3 ec 		;
	ld b,007h		;0045	06 07 		;Number of times to loop
	ld hl,l000bh		;0047	21 0b 00 	;Point to instruction to copy
	ld de,04000h		;004a	11 00 40 	;Point to RAM destination of RST 8 jump
l004dh:
	ld c,003h		;004d	0e 03 		;Number of bytes to copy
l004fh:
	ld a,(hl)		;004f	7e 		;Copy three bytes from (HL) to (DE)...	
	ld (de),a		;0050	12		;these are the instructions that...	
	inc de			;0051	13 		;the RSTs will jump to.
	inc hl			;0052	23 		;
	dec c			;0053	0d 		;
	jr nz,l004fh		;0054	20 f9 	  	;Copy second and third bytes of instruction
	ld a,l			;0056	7d 		;Add 5 to HL, so it points to the next...
	add a,005h		;0057	c6 05 		;instruction to copy into RAM.
	ld l,a			;0059	6f 		;
	djnz l004dh		;005a	10 f1 		;Loop until all 7 instructions are copied
	ld sp,040a0h		;005c	31 a0 40 	;Set up stack pointer
	ld c,088h		;005f	0e 88 		;CRTC Address Register
	jr l00c1h		;0061	18 5e 		;Skip over data, and continue from there
    
l0063h:
	ld c,c			;0063	49 I		;Extension for ROM image file
	ld c,c			;0064	49 I	
	ld c,c			;0065	49 I	
	jp 04015h		;0066	c3 15 40 	;NMI -- Jump here (and proceed) if NMI occurs
    
l0069h:
	jp l0a48h		;0069	c3 48 0a 	;This block is copied to RAM, 4015H-4047H
    
    defb    'MODEL%  '
	xor a			;0074	af 		;Last part of boot -- ends up at 4020H
	out (09ch),a		;0075	d3 9c 		;Switch boot ROM out...
	jp 04300h		;0077	c3 00 43 	;and start executing boot sector.
    
	nop			;007a	00 		;
	ld h,(hl)		;007b	66 	f	 ;Ends up at 4027H -- pointless LDs, which...
	ld h,h			;007c	64 	d 	;are the author's initials in lowercase.
	ld l,c			;007d	69 	i	 ;Executed when fatal error occurs, and...
	halt			;007e	76 	v	 ;<==this instruction ends the boot procedure.
	nop			;007f	00 		;
l0080h:
    defb    '1(16) 18-Oct-83', 0
	nop			;0090	00 		;
	nop			;0091	00 		;
	xor a			;0092	af 		;Ends up at 403EH after block copy
	out (09ch),a		;0093	d3 9c 		;Switch boot ROM out...
	ld hl,(l000bh)		;0095	2a 0b 00 	;Get word from from ROM image, or...
	inc a			;0098	3c 		;whatever's there instead.
	out (09ch),a		;0099	d3 9c 		;Boot ROM back in, and return
	ret			;009b	c9 		;This is the end of the block copied to RAM.
    
l009ch:
    defb    'Boot ROM Version Is ', 0 ;Message terminated with zero byte
    
l00b1h:				;	CRTC register values. Note no '0  Horizontal Total'
	nop			;00b1	00 		;1  Horizontal Displayed (80 char.)
	nop			;00b2	00 		;2  H. Sync Position
	nop			;00b3	00 		;3  H. Sync Width
	nop			;00b4	00 		;4  Vertical Total (25 lines)	
	nop			;00b5	00 		;5  V. Total Adjust
l00b6h:
	nop			;00b6	00 		;6  Vertical Displacement
	nop			;00b7	00 		;7  V.Sync Position
l00b8h:
	nop			;00b8	00 		;8  Interlace Mode
	nop			;00b9	00 		;9  Max Scan Line Address
	nop			;00ba	00 		;a  Cursor Start
	nop			;00bb	00 		;b  Cursor End
	nop			;00bc	00 		;c  Start Address (H)
	nop			;00bd	00 		;d  Start Address (L)
	nop			;00be	00 		;e  Cursor (H)
l00bfh:
	nop			;00bf	00 		;f  Cursor (L)
l00c0h:
	nop			;00c0	00 	. 
l00c1h:
	im 1			;00c1	ed 56 		;Z80 was set to IM 0 by RESET
	ld a,0d0h		;00c3	3e d0 		;Terminate command w/o interrupt
	out (0f0h),a		;00c5	d3 f0 		;to FDC 
	ld b,00fh		;00c7	06 0f 		;Number of CRTC registers to program
	ld hl,l00bfh		;00c9	21 bf 00 	;Point to data for register 15
l00cch:
	ld a,(hl)		;00cc	7e 		;Get data for CRTC register
	out (c),b		;00cd	ed 41 		;Send register number to CRTC addr register
	out (089h),a		;00cf	d3 89 		;Send data to CRTC data register
	dec hl			;00d1	2b 		;Point to next byte of data
	djnz l00cch		;00d2	10 f8 		;Repeat for next register, down to R1
	ld a,(03820h)		;00d4	3a 20 38 	;Keyboard column 5
	bit 6,a			;00d7	cb 77 		;<.> pressed?
	jp nz,l0cf2h		;00d9	c2 f2 0c 	;If yes, then go to RAM test
	ld a,080h		;00dc	3e 80 		;Set second page of video memory...
	out (084h),a		;00de	d3 84 		;
	call sub_0206h		;00e0	cd 06 02 	;and clear it.
	ld a,000h		;00e3	3e 00 		;Back to first page of video memory
	out (084h),a		;00e5	d3 84 		;
	call sub_0206h		;00e7	cd 06 02 	;Clear that too
	ld de,04015h		;00ea	11 15 40 	;Point to destination in RAM for block copy
	ld hl,l0069h		;00ed	21 69 00 	;Point to source, in ROM
	ld bc,l0033h		;00f0	01 33 00 	;Length of block
	ldir			;00f3	ed b0 		;Copy it into RAM
l00f5h:
	ld hl,04055h		;00f5	21 55 40 	;Point to boot ROM's data area in RAM...
	ld (hl),000h		;00f8	36 00 		;and clear it.
	ld d,h			;00fa	54	 	 ;
	ld e,l			;00fb	5d 		 ;
l00fch:
	inc de			;00fc	13 	 	;
	ld bc,00015h		;00fd	01 15 00 	;Number of bytes to clear, minus one
	ldir			;0100	ed b0 	 	;
	ld bc,07fffh		;0102	01 ff 7f 	;Number of times to scan keyboard...
	rst 30h			;0105	f7 		;so scan the keyboard.
	push af			;0106	f5 		;Save the result
	ld a,001h		;0107	3e 01 		;Floppy disk drive
	ld b,004h		;0109	06 04 		;Restore -- drive 0
	rst 8			;010b	cf 		;Do disk I/O
	jr z,l0115h		;010c	28 07 		;Jump if no errors...
	ld (04067h),a		;010e	32 67 40	 ;else store error code.
	cp 006h			;0111	fe 06 		;Was the error "Floppy not available"?
	jr z,l011ah		;0113	28 05 		;If yes, then skip next three lines 
l0115h:
	ld b,032h		;0115	06 32 		;Restore drives 1,2,3 (if possible)
	ld a,001h		;0117	3e 01 		;Floppy disk drive
	rst 8			;0119	cf 		;Do disk I/O
l011ah:
	pop af			;011a	f1 		;Retrieve result of keyboard scan
	jr z,l0163h		;011b	28 46 		;If no keys pressed, then try everything
	ld a,(04059h)		;011d	3a 59 40 	;Get value stored by kbd scan
	cp 086h			;0120	fe 86 		;Was it either <F1> or <1>?
	jr nz,l0129h		;0122	20 05 		;If not, then jump...
	call sub_01c0h		;0124	cd c0 01 	;else try HD boot -- returns only if...
	jr l0130h		;0127	18 07 		;unsuccessful, so jump (error no. in A).
l0129h:
	cp 087h			;0129	fe 87 		;Was either <F2> or <2> pressed?
	jr nz,l0135h		;012b	20 08 	 	 ;If not, skip next few lines
	call sub_01cah		;012d	cd ca 01 	 ;Try floppy boot (returns only if error)
l0130h:
	or a			;0130	b7 		;Error in HD or floppy boot
	jr z,l0137h		;0131	28 04 		;If error 0 (Mod III disk), then jump...
	jr l0151h		;0133	18 1c 		;else display fatal error and halt.
l0135h:
	cp 088h			;0135	fe 88 		;Was either <F3> or <3> pressed?
l0137h:
	jr z,l0175h		;0137	28 3c 		;If yes, then try Model III boot
	cp 080h			;0139	fe 80 		;Was <V> (display ROM version) pressed?
	jr nz,l014ah		;013b	20 0d 	 	;If not, then skip next few lines
	ld de,l0000h		;013d	11 00 00 	;Display at top of screen
	ld hl,l009ch		;0140	21 9c 00 	;'Boot ROM Version Is '
	rst 10h			;0143	d7 		;Display it
	ld hl,l0080h		;0144	21 80 00 	;Point to boot ROM version...
	rst 10h			;0147	d7 		;and display it...
	jr l0160h		;0148	18 16 		;and halt.
l014ah:
	cp 083h			;014a	fe 83 		;Was <Right Shift> pressed?
	jp z,l0c1ah		;014c	ca 1a 0c	;If yes, then jump to RS-232 boot...
	jr l00f5h		;014f	18 a4 		;else clear data, scan kbd, repeat.
l0151h:
	add a,a			;0151	87 		;Fatal Error: Multiply error number by 2
	ld hl,l0273h		;0152	21 73 02 	;Start of list for messages
	ld d,000h		;0155	16 00 		;Error number times two into DE
	ld e,a			;0157	5f 		;
	add hl,de		;0158	19 		;Point to appropriate entry in list
	ld a,(hl)		;0159	7e 		;Get start of table for message into HL
	inc hl			;015a	23 		;
	ld h,(hl)		;015b	66 		;
	ld l,a			;015c	6f  		;
	xor a			;015d	af 		;Set Z bec. we have starting position for msg
	ld e,a			;015e	5f 		;D is still 0 -- 0000H means top of screen
	rst 18h			;015f	df 		;Display message...
l0160h:
	jp 04027h		;0160	c3 27 40 	;and HALT.  End of an unsuccessful boot!
l0163h:
	ld a,055h		;0163	3e 55 	U 	;No keys were pressed during keyboard scan:
	ld b,a			;0165	47 		;Take a 'U'...
	out (0b6h),a		;0166	d3 b6 		;send it out, and see if there's an...
	in a,(0b6h)		;0168	db b6 		;Arcnet board to echo it.
	cp b			;016a	b8 		;Is an Arcnet board attached?
	call z,sub_0c15h	;016b	cc 15 0c 	;If it is, try Arcnet boot
	call sub_01c0h		;016e	cd c0 01 	;Try hard drive boot (no return if success)
	or a			;0171	b7 		;Ignore error 0 (M3 disk or 512-byte sector)
	call nz,sub_01cah	;0172	c4 ca 01	 ;If other error, try floppy boot
l0175h:
	ld a,(04066h)		;0175	3a 66 40 	;Model III boot:
	or a			;0178	b7 		;Was <L> pressed?
	jr nz,l0180h		;0179	20 05 	  	;If it was, skip next two lines
	call sub_01ebh		;017b	cd eb 01 	 ;Is ROM image already loaded?
	jr z,l01a2h		;017e	28 22 		;If it is, then skip next section
l0180h:
	ld a,(04067h)		;0180	3a 67 40 	;Error code from restore cmd on floppy 0
	or a			;0183	b7 		;Was there an error?
	jr nz,l0151h		;0184	20 cb 	 	 ;If so, display error message and halt
	ld a,001h		;0186	3e 01 		;Write-enable 0000H-37FFH
	out (084h),a		;0188	d3 84 		;Read boot ROM, but write to RAM
	ld de,l0000h		;018a	11 00 00 	;Display message at top of screen
	ld hl,l0491h		;018d	21 91 04 	;'Loading ROM Image...'
	rst 18h			;0190	df 		;Display it
	call sub_0a72h		;0191	cd 72 0a 	;Load ROM image from floppy disk
	push af			;0194	f5 		;Save registers...
	push hl			;0195	e5 		; 
	call sub_0206h		;0196	cd 06 02 	;clear screen (erase 'Loading...' msg)...
	pop hl			;0199	e1 		;
	pop af			;019a	f1 		;restore registers.
	jr nz,l0151h		;019b	20 b4 		;If error loading image, display msg & halt
	ld a,001h		;019d	3e 01 		;Indicate ROM image was just loaded
	ld (04058h),a		;019f	32 58 40 	;Store it
l01a2h:
	ld (04024h),hl		;01a2	22 24 40 	;Put transfer addr for ROM image...
	ld a,(04065h)		;01a5	3a 65 40 	;into last part of boot routine.
	or a			;01a8	b7 	;
	jr z,l01b6h		;01a9	28 0b 		;If <P> was NOT pressed, skip next few lines
	ld hl,l0443h		;01ab	21 43 04 	;'ROM image loaded -- press ENTER or BREAK'
	rst 18h			;01ae	df 		;Display it
l01afh:
	ld a,(03840h)		;01af	3a 40 38 	;Keyboard matrix -- column 6
	and 005h		;01b2	e6 05 		;Loop until <ENTER> or <BREAK>...
	jr z,l01afh		;01b4	28 f9 		;the ROM image will check for <BREAK>.
l01b6h:
	xor a			;01b6	af 		;Set 0000H-37FFH back to read-only...
	out (084h),a		;01b7	d3 84 		;still using Model III memory map.
	ld a,010h		;01b9	3e 10 		;2 MHz clock, I/O bus enabled
	out (0ech),a		;01bb	d3 ec 		;
	jp 04020h		;01bd	c3 20 40 	;Jump to last portion of boot code
sub_01c0h:
	ld a,002h		;01c0	3e 02 		;Hard Drive Boot:
	ld b,001h		;01c2	06 01 		;Initialize & restore 
	rst 8			;01c4	cf 		;Do disk I/O
	ret nz			;01c5	c0 		;Return if any errors...
	ld a,002h		;01c6	3e 02 		;else indicate HD...
	jr l01d0h		;01c8	18 06 		;and attempt to boot.
sub_01cah:
	ld a,(04067h)		;01ca	3a 67 40 	;Error code from floppy restore cmd
	or a			;01cd	b7 		;
	ret nz			;01ce	c0 		;If there had been any errors, then return...
	inc a			;01cf	3c 		;A=1 indicates floppy I/O
l01d0h:
	ld bc,l0c01h		;01d0	01 01 0c 	;0CH=Read, 01H=sector to read
	ld (04055h),a		;01d3	32 55 40 	;Store boot type (1 for floppy, 2 for HD)
	ld de,l0000h		;01d6	11 00 00 	;Cylinder to read
	ld hl,04300h		;01d9	21 00 43 	;Address of buffer for sector
	rst 8			;01dc	cf 		;Do disk I/O
	ret nz			;01dd	c0 		;Return if any errors
	ld a,e			;01de	7b 		;Length of sector that was read...
	ld (04056h),a		;01df	32 56 40 	;1 for 256 bytes, 2 for 512 -- store it.
	dec a			;01e2	3d 		;Was it a 256-byte sector?
	call z,sub_0215h	;01e3	cc 15 02 	;If so, see if it needs ROM image
	jr z,l01b6h		;01e6	28 ce 		;If ROM image NOT needed, then jump & boot...
	ld a,000h		;01e8	3e 00 		;else 'ROM Image Not Found'...
	ret			;01ea	c9 		;and return (NZ set).
sub_01ebh:
	ld hl,03000h		;01eb	21 00 30 	;Check if ROM image is in memory:
	ld b,00ah		;01ee	06 0a 		;Start at 3000H
	ld a,0c3h		;01f0	3e c3 		;Opcode for JP instruction
l01f2h:
	cp (hl)			;01f2	be 		;Do we have a JP?
	ret nz			;01f3	c0 		;If not, no ROM image in memory
	inc hl			;01f4	23 		;Point to third byte following...
	inc hl			;01f5	23 		;make sure that the ROM image's...
	inc hl			;01f6	23 		;jump table is in place.
	djnz l01f2h		;01f7	10 f9 		;Check ten times in all
	call 0403eh		;01f9	cd 3e 40 	;Code had been copied from 0092H to 403EH...
	xor a			;01fc	af 		;it gets the word at 000BH from ROM image.
	ld de,0e9e1h		;01fd	11 e1 e9 	;Value that ought to be there for ROM image
	sbc hl,de		;0200	ed 52 		;Set Z if ROM image present, else NZ
	ld hl,03015h		;0202	21 15 30 	;Transfer address if image already loaded
	ret			;0205	c9 	;
sub_0206h:
	ld a,020h		;0206	3e 20 		;Clear Screen: Put space into A
	ld hl,03c00h		;0208	21 00 3c 	;Start of video memory
	ld bc,l03ffh		;020b	01 ff 03 	;Length of screen, minus one
	ld (hl),a		;020e	77 		;Put space at start of screen
	ld d,h			;020f	54 		;Set DE to...
	ld e,l			;0210	5d 		;
	inc de			;0211	13 		;HL plus one.
	ldir			;0212	ed b0 		;Voila!
	ret			;0214	c9 		;The screen has been cleared.
sub_0215h:
	ld a,(04064h)		;0215	3a 64 40 	;Address for recording <N> key
	or a			;0218	b7 		;Was <N> key pressed?
	jr nz,l0228h		;0219	20 0d 	  	;If yes, don't check if ROM image is needed
	ld hl,04300h		;021b	21 00 43 	;Point to start of the boot sector we read
	ld b,0feh		;021e	06 fe 		;Number of bytes to check
l0220h:
	ld a,0cdh		;0220	3e cd 		;Opcode for CALL instruction
	cp (hl)			;0222	be 		;Look in boot sector for a CALL
	jr z,l023fh		;0223	28 1a 		;Jump if we find one...
l0225h:
	inc hl			;0225	23 		;else point to next byte...	
	djnz l0220h		;0226	10 f8 		;and keep looking.
l0228h:
	call sub_01ebh		;0228	cd eb 01 	;Is ROM image in memory?
	jr nz,l023dh		;022b	20 10 	  	;Jump if not, because we won't need to...
	ld a,001h		;022d	3e 01 		;destroy the ROM image.
	out (084h),a		;022f	d3 84 		;Write-enable that part of RAM
	ld hl,03000h		;0231	21 00 30 	;Starting at 3000H...
	ld d,h			;0234	54 		;
	ld e,l			;0235	5d 		;
	ld (hl),l		;0236	75 		;put zeros into...
	inc de			;0237	13 		;
	ld bc,l001eh		;0238	01 1e 00 	;1EH consecutive bytes...
	ldir			;023b	ed b0 		;from 3000H to 301DH.
l023dh:
	xor a			;023d	af 		;Set Z flag to indicate that we won't need...
	ret			;023e	c9 		;to load the ROM image, and RETurn.
l023fh:
	inc hl			;023f	23 		;Point to next byte in boot sector...
	dec b			;0240	05 		;and decrement byte counter.
	inc hl			;0241	23 		;Point to second byte after "CALL"...
	dec b			;0242	05 		;which is the MSB of the CALL's destination.
	ld a,(hl)		;0243	7e 		;
l0244h:
	or a			;0244	b7 		;Is it a call to page 0? (CALL 00xxH)
	jr nz,l0225h		;0245	20 de 	  	;If not, keep looking through boot sector...
	or l			;0247	b5 		;else set NZ to indicate we'll need the...
	ret			;0248	c9 		;ROM image, and RETurn.
l0249h:
	ld a,d			;0249	7a 		;RST 10H -- Display String
	or 03ch			;024a	f6 3c 		;Point DE to video memory
	ld d,a			;024c	57 		;
l024dh:
	ld a,(hl)		;024d	7e 		;Get character to display
	or a			;024e	b7 		;Is it zero?
	ret z			;024f	c8 		;If zero, then we're done displaying it
	ld (de),a		;0250	12 		;Put character into video memory
	inc hl			;0251	23 		;Next character to display
	inc de			;0252	13 		;Next byte in video memory
	jr l024dh		;0253	18 f8 		;Repeat for next character, until done
l0255h:
	jr z,l025bh		;0255	28 04 		;RST 18H -- Display Message: If Z, then...
l0257h:
	ld e,(hl)		;0257	5e 		;we already have display position in DE...
	inc hl			;0258	23 		;else HL points to it, and we must...
	ld d,(hl)		;0259	56 		;read it into DE.
	inc hl			;025a	23 		;
l025bh:
	ld b,h			;025b	44 		;Now BC points to the...
	ld c,l			;025c	4d 		;table of strings to display.
l025dh:
	ld a,(bc)		;025d	0a 		;Get first value from table into HL
	inc bc			;025e	03 		;
	ld l,a			;025f	6f 		;
	ld a,(bc)		;0260	0a 		;
	inc bc			;0261	03 		;
	ld h,a			;0262	67 		;
	inc a			;0263	3c 		;Does MSB = 0FFH?
	jr nz,l0270h		;0264	20 0a 	  	;Jump if not (display string & loop again)
	ld a,l			;0266	7d 		;
	inc a			;0267	3c 		;Does LSB = 0FFH?
	ret z			;0268	c8 		;If it does, we're done
	inc a			;0269	3c 		;Does LSB = 0FEH?
	jr nz,l0270h		;026a	20 04 	  	;Jump if not (display string & loop again)
	push bc			;026c	c5 		;Must be 0FFFEH...load pointer to...
	pop hl			;026d	e1 		;table of strings into HL...
	jr l0257h		;026e	18 e7 		;get display position for next part of msg.
l0270h:
	rst 10h			;0270	d7 		;Display string that HL points to...
	jr l025dh		;0271	18 ea 		;and repeat for next value in table.
    
l0273h:
    defw    err0pte ;0273	8d 		;Points to phrases for Error 0 message
    defw    002b7h	;0275	b7 		;Ditto for Error 1
	defw    002dbh	;0277	db 02 		;Error 2
	defw    00303h	;0279	03 		;Error 3
	defw    00325h	;027b	25 		;Error 4
	defw    00349h	;027d	49 		;Error 5
	defw    0036fh	;027f	6f 		;Error 6
    defw    00397h	;0281	97 		;Error 7
	defw    003b1h	;0283	b1 		;Error 8
	defw    003d5h	;0285	d5 		;Error 9
	defw    003f7h	;0287	f7 		;Error 10 (0AH)
	defw    00437h	;0289	37 		;Error 11 (0BH)
	defw    0043dh	;028b	3d 		;Error 12 (0CH)
    
err0pte:
	defw    l04b7h	;028d	b7 		;Error 0 -- pointers to phrases: 'The '
	defw    l04c5h	;028f	c5 		;'ROM Image '
	defw    l04d0h	;0291	d0 		;'Was Not '
	defw    l04d9h	;0293	d9 		;'Found On Drive '
	defw    l04e9h	;0295	e9 		;'0'
	defw    0fffeh	;0297	fe ff 	 	;End of this part of message
err0ptd: 
    defw    l00c0h	;0299	c0 		;Display next part of msg at row 3, col 0
	defw    l04c5h	;029b	c5 		;'ROM Image '
	defw    l05eah	;029d	ea 05 f1 	;'Nicht ' ;'Gefunden Auf '
    defw    l05f1h
	defw    l0609h	;02a1	09 		;'Laufwerk ' ;'0'
    defw    l04e9h
	defw    0fffeh	;02a5	fe ff 		;End of this part of message
err0ptf:    
    defw    l0180h	;02a7	80 		;Display next part at row 6, col 0
	defw    l04c9h	;02a8	01 c9 04 	;'Image '
    defw    l07ddh  ;02ab	dd 07 00 	;'ROM ' ;'Absent'
	defw    l0700h	;02ae	07 		
	defw    l0707h	;02af	07 		;'e Du '
	defw    l0771h	;02b1	71 		;'Disque'
	defw    l070dh	;02b3	0d 		;' Dans l`Unite 0'
	defw    0ffffh	;02b5	ff 		;0FFFFH = End of message
l02b7h:  
	defw	l04ebh	;02b7	eb 		;Error 1: 'Arcnet '
	defw	l04f3h	;02b9	f3 		;'Boot '
	defw    l04f9h	;02bb	f9 		;'Is Not '
	defw    l0501h	;02bd	01 05 fe 	;'Available '
	defw    0fffeh	;02bf	fe ff 		

l02c1h:
    defw     l00c0h   
    defw     l04ebh    		;'Arcnet '
    defw     l04f3h   		;'Boot '
    defw     l05e6h    		;'Ist Nicht '
    defw     l06f0h    		;'Im System'
    defw     0fffeh   
l02cdh:    
    defw     l0180h   
    defw     l04f3h    		;'Boot '
    defw     l04ebh   		;'Arcnet '
    defw     l0700H   		;'Absent'
    defw     l0708H   		;' Du '
    defw     l071dh   		;'Systeme '
    defw     0ffffh   
l02dbh:
    defw     l04c5h   		; Error 2: 'ROM Image '
    defw     l050ch  		;'Can`t Be Loaded - '
    defw     l051fh   		;'Too Many Extents'
    defw     0fffeh   
l02e3h:    
    defw     l00c0h   		;'ROM Image '
    defw     l04c5h   
    defw     l0613h   		;'Kann '
    defw     l05eah   	 	;'Nicht '
    defw     l061dh   		;'Geladen '
    defw     l0653h   		;'Werden '
    defw     l051ch   		;'- '
    defw     l065bh   		;'Zu Viele Bereiche'
    defw     0fffeh   
l02f5h:
    defw     l0180h   		;'Image '
    defw     l04c9h  	 	;'ROM ' ;
    defw     l07ddh   		;'Non Chargeable '
    defw     l0726h   		;'-
    defw     l051ch   		;'Segments Trop Numbreux'
    defw     l0736h   
    defw     0ffffh   
l0303h:    
    defw     l04b7h   		; Error 3: 'The '
    defw     l0538h   		;'Hard Disk '	
    defw     l04e2h   		;'Drive ' 
    defw     l04f9h    		;'Is Not '
    defw     l0550h   		;'Ready '
    defw     0fffeh   
l030fh: 
    defw     l00c0h   
    defw     l0643h   
    defw     l05e6h   
    defw     l066dh   
    defw     0fffeh   
l0319h:
    defw     l0180h   
    defw     l074dh   
    defw     l0771h   
    defw     l0781h   
    defw     l0787h   
    defw     0ffffh   
l0325h:
    defw     l04b7h   
    defw     l0530h   
    defw     l053dh   
    defw     l04e2h   
    defw     l04f9h   
    defw     l0550h   
    defw     0fffeh   
l0333h:
    defw     l00c0h   
    defw     l05ffh   
    defw     l05eah   
    defw     l066dh   
    defw     0fffeh   
l033dh:
    defw     l0180h
    defw     l074dh   
    defw     l0771h   
    defw     l0778h   
    defw     l0787h   
    defw     0ffffh   
l0349h:
    defw     l04b7h   
    defw     l0538h   
    defw     l04e2h   
    defw     l04f9h   
    defw     l0501h   
    defw     0fffeh   
l0355h:
    defw     l00c0h   
    defw     l0643h   
    defw     l05e6h   
    defw     l06f0h   
    defw     0fffeh   
l035fh:
    defw     l0180h   
    defw     l074dh   
    defw     l0771h   
    defw     l0781h   
    defw     l0700h   
    defw     l0708h   
    defw     l071dh   
    defw     0ffffh   
l036fh:
    defw     l04b7h   
    defw     l0530h   
    defw     l053dh   
    defw     l04e2h   
    defw     l04f9h   
    defw     l0501h   
    defw     0fffeh   
l037dh:
    defw     l00c0h   
    defw     l05ffh   
    defw     l05e6h   
    defw     l06f0h   
    defw     0fffeh   
l0387h:
    defw     l0180h   
    defw     l074dh   
    defw     l0771h   
    defw     l0778h   
    defw     l0700h   
    defw     l0708h   
    defw     l071dh   
    defw     0ffffh   
l0397h:
    defw     l0557h   
    defw     l04b7h   
    defw     l0530h   
    defw     l04e2h   
    defw     l055eh   
    defw     0fffeh   
l03a3h:    
    defw     l00c0h   
    defw     l05ffh   
    defw     l0675h   
    defw     0fffeh   
l03abh:
    defw     l0180h   
    defw     l0792h   
    defw     0ffffh   
l03b1h:
    defw     l057dh   
    defw     l0587h   
    defw     l058dh   
    defw     l0567h   
    defw     l0590h   
    defw     l053dh   
    defw     0fffeh   
l03bfh:
    defw     l00c0h   
    defw     l057dh   
    defw     l0698h   
    defw     l06a1h   
    defw     0fffeh   
l03c9h:
    defw     l0180h   
    defw     l07bbh   
    defw     l07b5h   
    defw     l07aah   
    defw     l075bh   
    defw     0ffffh   
l03d5h:
    defw     l0582h   
    defw     l058dh   
    defw     l0567h   
    defw     l0590h   
    defw     l053dh   
    defw     0fffeh   
l03e1h:
    defw     l00c0h   
    defw     l06fah   
    defw     l0698h   
    defw     l06a1h   
    defw     0fffeh   
l03ebh:
    defw     l0180h   
    defw     l07bbh   
    defw     l07c3h   
    defw     l07aah   
    defb     05bh   
l03f4h:    
    defb     007h   
    defw     0ffffh   
l03f7h:
    defw     l04b7h   
    defw     l04c5h   
    defw     l050ch   
    defw     l0530h   
l03ffh:    
    defw     l053dh   
    defw     l04e2h   
    defw     l04f9h   
    defw     l0550h   
    defw     0fffeh   
l0409h:
    defw     l00c0h   
    defw     l04c5h   
    defw     l0613h   
    defw     l05eah   
    defw     l061dh   
    defw     l0653h   
    defw     l051ch   
    defw     l05ffh   
    defw     0fffeh   
l041bh:
    defw     0100h   
    defw     l05e6h   
    defw     l066dh   
    defw     0fffeh   
l0423h:
    defw     l0180h   
    defw     l04c9h   
    defw     l07ddh   
    defw     l0726h   
    defw     l051ch   
    defw     l074dh   
    defw     l0771h   
    defw     l0778h   
    defw     l0787h   
    defw     0ffffh   
l0437h:
    defw     l0572h   
    defw     l0587h   
    defw     0ffffh   
l043dh:
    defw     l05e2h   
    defw     l0587h   
    defw     0ffffh   
l0443h:
    defw     00000h   
    defw     l04b7h   
    defw     l04c5h   
    defw     l05a0h   
    defw     l0514h   
    defw     l05a9h   
    defw     0fffeh   
l0451h:
    defw     00040h   
    defw     l05c7h   
    defw     l05d6h   
    defw     l0543h   
    defw     0fffeh   
l045bh:
    defw     l00c0h   
    defw     l04c5h   
    defw     l0619h   
    defw     l051ch   
    defw     l0626h   
    defw     l05cdh   
    defw     0fffeh   
l0469h:
    defw     0100h   
    defw     l06d7h   
    defw     l05d9h   
    defw     l06ceh   
    defw     0fffeh   
l0473h:
    defw     l0180h   
    defw     l07d1h   
    defw     l07dah   
    defw     l07e2h   
    defw     l051ch   
    defw     l07ebh   
    defw     l0771h   
    defw     l07f7h   
    defw     0fffeh   
l0485h:
    defw     01c0h   
    defw     l05cdh   
    defw     l0808h   
    defw     l05d9h   
    defw     l080ch   
    defw     0ffffh   
l0491h:
    defw     004bch   
    defw     l04c5h   
    defw     l051ch   
    defw     l05bbh   
    defw     0fffeh   
l049bh:
    defw     00c0h   
    defw     l04c5h   
    defw     l06ddh   
    defw     l061dh   
    defw     l051ch   
    defw     l06e3h   
    defw     0fffeh   
l04a9h:
    defw     l0180h   
    defw     l081bh   
    defw     l07d1h   
    defw     l07ddh   
    defw     l051ch   
    defw     l082ah   
    defw     0ffffh   

    
l04b7h:    
    defb    'The ', 0
l04bch:
	defb	'Loading ', 0
l04c5h:
	defb	'ROM '
l04c9h:
    defb    'Image ', 0
l04d0h:
	defb	'Was Not ', 0
l04d9h:
	defb	'Found On '
l04e2h:
    defb    'Drive ', 0
l04e9h:
	defb	'0', 0
l04ebh:
	defb	'Arcnet ', 0
l04f3h:
	defb	'Boot ', 0
l04f9h:
	defb	'Is Not ', 0
l0501h:
	defb	'Available ', 0
l050ch:
	defb	'Can', 027h, 't Be'
l0514h:
    defb    ' Loaded '
l051ch:
    defb    '- ', 0
l051fh:
	defb	'Too Many Extents', 0
l0530h:
	defb	'Floppy ', 0
l0538h:
	defb	'Hard '
l053dh:
    defb    'Disk ', 0
l0543h:
	defb	'When You Are '
l0550h:
    defb    'Ready ', 0
l0557h:
	defb	'Close ', 0
l055eh:
	defb	'Door'
l0562h:
	defb	' And '
l0567h:
    defb    'Try Again ', 0
l0572h:
	defb	'Lost Data ', 0
l057dh:
	defb	'CRC ', 0
l0582h:
    defb    'Seek '
l0587h:
    defb    'Error', 0
l058dh:
    defb    ', ', 0
l0590h:
	defb	'Or Use Another ', 0
l05a0h:
	defb	'Has Been', 0
l05a9h:   
	defb	'Switch Disks And ', 0
l05bbh:
	defb	'Please Wait', 0
l05c7h:
	defb	'Press '
l05cdh:
    defb    '<Enter> ', 0
l05d6h:
	defb	'Or '
l05d9h:
    defb    '<Break> ', 0
l05e2h:
	defb	'ID ', 0
l05e6h:
    defb    'Ist '
l05eah:    
    defb    'Nicht ', 0
l05f1h:
    defb    'Gefunden Auf ', 0
l05ffh:
    defb    'Disketten-'
l0609h:
    defb    'Laufwerk ', 0
l0613h:
    defb    'Kann ', 0
l0619h:
    defb    'Ist '
l061dh:
    defb    'Geladen ', 0
l0626h:
    defb    'Disketten Wechseln Und Dann ', 0
l0643h:
    defb    'Die Festplatte ', 0
l0653h:
    defb    'Werden ', 0
l065bh:
    defb    'Zu Viele Bereiche', 0
l066dh:
    defb    'Bereit ', 0
l0675h:
    defb    'Schlie', 004h, 'en Und Erneut Starten', 0
l0692h:
    defb    'Durda', 0
l0698h:    
    defb    'Fehler, ', 0
l06a1h:
    defb    'Neu Starten Oder Eine Andere Platte Benutzen', 0 
l06ceh:
    defb    'Dr', 00ah, 'cken ', 0
l06d7h:
    defb    'Oder ', 0
l06ddh:
    defb    'Wird ', 0
l06e3h:
    defb    'Bitte Warten', 0
l06f0h:  
    defb    'Im System', 0
l06fah:
    defb    'Such ', 0
l0700h:
    defb    'Absent', 0
l0707h:
    defb    'e'
l0708h:
    defb    ' Du ', 0
l070dh:
    defb    ' Dans l', 027h, 'Unit', 003h, ' 0', 0
l071dh:
    defb    'Syst', 00Ch, 'me ', 0
l0726h:
    defb    'Non Chargeable ', 0
l0736h:
    defb    'Segments Trop Numbreux', 0
l074dh:
    defb    'Unit', 003h, ' ', 0
l0754h:
    defb    'Frank ', 0
l075bh:
    defb    'Ou Utilisez Une Autre '
l0771h:    
    defb    'Disque', 0
l0778h:  
    defb    '-Souple ', 0
l0781h:
    defb    '-Dur ', 0
l0787h:
    defb    'Non Pr', 012h, 'te ', 0
l0792h:
    defb    'Fermez Porte d', 027h, 'Unit', 003h, ' Et '
l07aah:    
    defb    'R', 003h, 'essayez ', 0
l07b5h:
    defb    'CRC, ', 0
l07bbh:
    defb    'Erreur ', 0
l07c3h:
    defb    'De Chercher, ', 0
l07d1h:
    defb    'l', 027h, 'Image ', 0
l07dah:
    defb    'Du '
l07ddh:
    defb    'ROM ', 0
l07e2h:
    defb    'Charg', 003h, 'e ', 0
l07ebh:
    defb    'Changez De ', 0
l07f7h:
    defb    ' Et Appuyez Sur ', 0
l0808h:
    defb    'Ou ', 0
l080ch:
    defb    'Pour Continuer', 0
l081bh:
    defb    'Chargement De ', 0
l082ah:
    defb    'Veuillez Patienter ', 0
;	jr nz,l083eh		;083c	20 00 	  	;RST 30H -- Keyboard scan: Get column 6
l083eh:
    
	ld a,(03840h)		;083e	3a 40 38 	;Is someone pressing <ENTER>?
	rrca			;0841	0f 		;Loop until user lets go of <ENTER> key
	jr c,l083eh		;0842	38 fa 		;Get col 0
l0844h:
	ld a,(03801h)		;0844	3a 01 38 	
	and 0feh		;0847	e6 fe 		;Any of bits 1-7 pressed? (<A>-<G>)
	jr z,l0857h		;0849	28 0c 		;Jump if none of those were pressed...
	ld d,0ffh		;084b	16 ff 		;else start with -1...
l084dh:
	inc d			;084d	14 		;and keep adding 1...
	rrca			;084e	0f 		;until we find which keyboard row...
	jr nc,l084dh		;084f	30 fc 		;was pressed.
	ld a,040h		;0851	3e 40 		;Add 40H to row number to get...
	add a,d			;0853	82 		;the ASCII code for that key (<A>-<G>).
	ld (0401dh),a		;0854	32 1d 40 	;Store ASCII code in name of ROM image file
l0857h:
	ld a,(03840h)		;0857	3a 40 38 	;Next, get column 6 of keyboard matrix
	bit 2,a			;085a	cb 57 		;Was <BREAK> pressed?
	jr z,l0861h		;085c	28 03 		;If not, skip next instruction...
	ld (0405bh),a		;085e	32 5b 40 	;else put a non-zero value into 405BH.
l0861h:
	bit 0,a			;0861	cb 47 		;Was <ENTER> pressed?
	jr nz,l08b0h		;0863	20 4b 	  	;If yes, then end of keyboard scan
	ld a,(03802h)		;0865	3a 02 38 	;Column 1
	bit 6,a			;0868	cb 77 		;Was <N> pressed?
	jr z,l086fh		;086a	28 03 		;Skip next instruction if not...
	ld (04064h),a		;086c	32 64 40 	;else store non-zero value to show it was.
l086fh:
	bit 4,a			;086f	cb 67 		;What about the <L> key?
	jr z,l0876h		;0871	28 03 		;
	ld (04066h),a		;0873	32 66 40 	;Store non-zero value if <L> was pressed
l0876h:
	ld d,080h		;0876	16 80 		;80H & greater show type of boot requested
	ld a,(03804h)		;0878	3a 04 38 	;Column 2
	bit 6,a			;087b	cb 77 		;Was <V> pressed?
	jr nz,l08a7h		;087d	20 28 	  	;If yes, can't use any other options
	bit 0,a			;087f	cb 47 		;Was <P> pressed?
	jr z,l0886h		;0881	28 03 		;Skip next instruction if it wasn't...
	ld (04065h),a		;0883	32 65 40 	;else indicate that it was.
l0886h:
	inc d			;0886	14 		;	
	inc d			;0887	14 		;
	ld a,(03880h)		;0888	3a 80 38 	;Column 7: L & R <SHIFT>, <CTRL>,...
	or a			;088b	b7 		;<CAPS>, <F1>, <F2>, <F3>
	jr z,l0894h		;088c	28 06 		;Jump if none of those were pressed
l088eh:
	rrca			;088e	0f 		;Add row number to D...
	jr c,l08a7h		;088f	38 16 		;and jump once that's been done.
	inc d			;0891	14 		
	jr l088eh		;0892	18 fa 		
l0894h:
	ld a,(03810h)		;0894	3a 10 38 	
	ld d,086h		;0897	16 86 		
	bit 1,a			;0899	cb 4f 		;Was <1> pressed?
	jr nz,l08a7h		;089b	20 0a 	  	;If yes, jump with D=86H
	inc d			;089d	14 		
	bit 2,a			;089e	cb 57 		;Was <2> pressed?
	jr nz,l08a7h		;08a0	20 05 	  	;If yes, jump with D=87H
	inc d			;08a2	14 		
	bit 3,a			;08a3	cb 5f 		;Was <3> pressed?
	jr z,l08abh		;08a5	28 04 		;If not, then jump, else D=88H
l08a7h:
	ld a,d			;08a7	7a 		;The value for boot option selected, 82H-88H
	ld (04059h),a		;08a8	32 59 40 	
l08abh:
	dec bc			;08ab	0b 		;Store it
	ld a,b			;08ac	78 		;the number of times to scan the keyboard.
	or c			;08ad	b1 		;Decrement it and see if it's down to 0
	jr nz,l0844h		;08ae	20 94 	  	;If not, then scan again
l08b0h:
	ld a,(04059h)		;08b0	3a 59 40 	;End of keyboard scan -- get boot option...
	or a			;08b3	b7 		;Set Z if no options were chosen, else NZ...
	ret			;08b4	c9 		;and that's the end of the keyboard scan.
l08b5h:
	dec a			;08b5	3d 		;RST 08H -- Disk I/O
	jp z,l096fh		;08b6	ca 6f 09 	;If A was 1, then floppy disk...
	ld a,b			;08b9	78 		;else use hard disk.
	cp 001h			;08ba	fe 01 		;What function was requested?
	jr nz,l08c3h		;08bc	20 05 	  	;If not "initialize", then skip next 2 lines
	call sub_0956h		;08be	cd 56 09 	;Reset HD controller board
	ld b,004h		;08c1	06 04 		;Change function to "restore"
l08c3h:
	ld a,c			;08c3	79 		;Sector to read
	out (0cbh),a		;08c4	d3 cb 		;Out to WDC sector register
	in a,(0cbh)		;08c6	db cb 		;See what we get back from there...
	cp c			;08c8	b9 		;If something different, no HD attached
	ld a,005h		;08c9	3e 05 		;'Hard Drive Not Available'
	jr nz,l08eeh		;08cb	20 21 	  	;If no HD available, then error
	xor a			;08cd	af 		;Drive 1, head 0, sectors 256 bytes
	out (0ceh),a		;08ce	d3 ce 		;Out to size/drive/head register
	ld a,d			;08d0	7a 		;Cylinder to read -- LSB
	out (0cdh),a		;08d1	d3 cd 		;Out to WDC cylinder low register
	ld a,e			;08d3	7b 		;Cylinder to read -- MSB
	out (0cch),a		;08d4	d3 cc 		;To WDC cylinder high register
	push bc			;08d6	c5 		;Save function, sector during next loop
	ld d,008h		;08d7	16 08 		;Counter for outer loop
l08d9h:
	ld bc,l0000h		;08d9	01 00 00 	;Counter for inner loop
l08dch:
	in a,(0cfh)		;08dc	db cf 		;Get WDC status register
	bit 6,a			;08de	cb 77 		;Is drive ready?
	jr nz,l08f0h		;08e0	20 0e 	  	;Jump if it's ready...
	dec bc			;08e2	0b 		;else decr counter for inner loop...
	ld a,b			;08e3	78 		;
	or c			;08e4	b1 		;
	jr nz,l08dch		;08e5	20 f5 	  	;and keep trying.
	dec d			;08e7	15 		;Decrement counter for outer loop...
	jr nz,l08d9h		;08e8	20 ef 	  	;try half a million times (about 6 secs)...
	pop bc			;08ea	c1 		;before concluding that drive isn't ready.
	ld d,003h		;08eb	16 03 		;'Hard Drive Not Ready'
l08edh:
	ld a,d			;08ed	7a 		;Error: error code into A...
l08eeh:
	or a			;08ee	b7 		;set NZ to indicate error...
	ret			;08ef	c9 		;and return from RST 8.
l08f0h:
	pop bc			;08f0	c1 		;Holds function and sector number
	ld a,b			;08f1	78 		;Function into A
	ld d,016h		;08f2	16 16 		;WDC "restore" command
	cp 004h			;08f4	fe 04 		;
	jr z,l08fah		;08f6	28 02 		;Jump if function is "restore"...
	ld d,070h		;08f8	16 70 		;else change WDC command to "seek".
l08fah:
	ld a,d			;08fa	7a 		;WDC command...
	out (0cfh),a		;08fb	d3 cf 		;out to command register.
	call sub_0962h		;08fd	cd 62 09 	;Wait until drive not busy & seek complete
	jr nz,l0935h		;0900	20 33 	  	;Jump if any errors
	ld a,b			;0902	78 		;Function into A again...
	cp 00ch			;0903	fe 0c 		;
	jr nz,l0933h		;0905	20 2c 	  	;if it wasn't "read sector", then we're done.
	ld a,020h		;0907	3e 20 		;WDC command to read single sector
	ld e,000h		;0909	1e 00 		;E will be counter for sector length
	out (0cfh),a		;090b	d3 cf 		;Command out to WDC
	call sub_0962h		;090d	cd 62 09 	;Wait until drive is not busy
	jr z,l092dh		;0910	28 1b 		;Jump if no errors reading sector
	ld d,c			;0912	51 		;Save sector number
	call sub_094ch		;0913	cd 4c 09 	;Get WDC status
	bit 4,c			;0916	cb 61 		;If error NOT "ID not found", then real error
	jr z,l0938h		;0918	28 1e 		;Otherwise, it may be a 512-byte sector
	ld a,020h		;091a	3e 20 		;Set size/drive/head to 512-byte sectors
	out (0ceh),a		;091c	d3 ce 		;
	ld a,d			;091e	7a 		;Sector number was saved in D...
	out (0cbh),a		;091f	d3 cb 		;send it to WDC sector register again.
	ld a,020h		;0921	3e 20 		;WDC command: read single sector
	out (0cfh),a		;0923	d3 cf 		;
	call sub_0962h		;0925	cd 62 09 	;Wait until drive not busy or seek complete
	jr nz,l0935h		;0928	20 0b 	  	;Jump if any errors
	call l092dh		;092a	cd 2d 09 	;Get 256 bytes twice (512-byte sector)
l092dh:
	ld bc,000c8h		;092d	01 c8 00 	;B is byte counter, C is port for input
	inir			;0930	ed b2 		;Get 256 bytes from WDC sector buffer
	inc e			;0932	1c 		;Increment MSB of byte counter
l0933h:
	xor a			;0933	af 		;Indicate no errors
	ret			;0934	c9 		;Return from RST 8
l0935h:
	call sub_094ch		;0935	cd 4c 09 	;Get WDC error register
l0938h:
	ld d,008h		;0938	16 08 		;'CRC Error'
	bit 6,a			;093a	cb 77 		;
	jr nz,l08edh		;093c	20 af 	  	;Jump if error was CRC data field error
	bit 5,a			;093e	cb 6f 		;(Documentation says bit 5 is always 0...
	jr nz,l08edh		;0940	20 ab 	  	;if it ever SHOULD be 1, then same error)
	ld d,00ch		;0942	16 0c 		;'ID Error'
	bit 4,a			;0944	cb 67 		;
	jr nz,l08edh		;0946	20 a5 	  	;Jump if ID not found...
	ld d,009h		;0948	16 09 		;else call it a 'Seek Error'
	jr l08edh		;094a	18 a1 		;
sub_094ch:
	bit 1,a			;094c	cb 4f 		;Is a command in progress?
	ld b,a			;094e	47 		;
	call nz,sub_0959h	;094f	c4 59 09 	;If so, reset HD controller board
	in a,(0c9h)		;0952	db c9 		;Get WDC error register...
	ld c,a			;0954	4f 		;and save it in C.
	ret			;0955	c9 		;
sub_0956h:
	xor a			;0956	af 		;Hard disk controller board:
	out (0c1h),a		;0957	d3 c1 		;reset control register...
sub_0959h:
	ld a,010h		;0959	3e 10 		;
	out (0c1h),a		;095b	d3 c1 		;reset controller board...
	ld a,00ch		;095d	3e 0c 		;
	out (0c1h),a		;095f	d3 c1 		;enable controller board...
	ret			;0961	c9 		;and return.
sub_0962h:
	in a,(0cfh)		;0962	db cf 		;Get HD status
	bit 7,a			;0964	cb 7f 		;
	jr nz,sub_0962h		;0966	20 fa 	  	;If HD is busy, keep looping
	bit 4,a			;0968	cb 67 		;
	jr z,sub_0962h		;096a	28 f6 		;If seek not complete, keep looping
	bit 0,a			;096c	cb 47 		;Set NZ if error
	ret			;096e	c9 		;
l096fh:
	ld a,0d0h		;096f	3e d0 		;Floppy Disk Driver:
	out (0f0h),a		;0971	d3 f0 		;Terminate command without interrupt
	call sub_0a58h		;0973	cd 58 0a 	;Delay
	ld a,081h		;0976	3e 81 		;DDEN, side 0, drive 0
	out (0f4h),a		;0978	d3 f4 		;Out to drive select
	ld a,c			;097a	79 		;Sector
	out (0f2h),a		;097b	d3 f2 		;Out to FDC Sector Register
	ld a,b			;097d	78 		;Function
	cp 005h			;097e	fe 05 		;
	jr nc,l09a2h		;0980	30 20 		;Jump if function > 4
	ld a,00ch		;0982	3e 0c 		;Restore -- verify track, 6 ms step
	out (0f0h),a		;0984	d3 f0 		;Out to FDC Command Register
	call sub_0a64h		;0986	cd 64 0a 	;Wait until not busy or not ready, get status
	ld b,a			;0989	47 		;
	and 0dfh		;098a	e6 df 		;Ignore bit 5 ("head loaded")
	cp 081h			;098c	fe 81 		;Unless we have "not ready" and "busy"...
	jr nz,l09e2h		;098e	20 52 	  	;then get error code (if any) and return.
	ld a,c			;0990	79 		;FDC status before "not ready"
	and 09eh		;0991	e6 9e 		;Did we have ready, not tk 0, no index?
	ld d,006h		;0993	16 06 		;Error 'Floppy Drive Not Available'
	jr z,l099fh		;0995	28 08 		;If so, floppy drive's not available.
	ld d,004h		;0997	16 04 		;Error 'Floppy Drive Not Ready'
	cp 006h			;0999	fe 06 		;Index pulse? (Index because no disk there!)
	jr z,l099fh		;099b	28 02 		;If so, 'Not Ready' (no disk), else..
	ld d,007h		;099d	16 07 		;no index bec. disk not turning: 'Close Door'
l099fh:
	ld a,d			;099f	7a 		;Put error code into A...
	or a			;09a0	b7 		;set Z/NZ (NZ indicates error)...
	ret			;09a1	c9 		;and return from RST 8.
l09a2h:
	cp 032h			;09a2	fe 32 		;If function wasn't "restore all"...
	jr nz,l09cdh		;09a4	20 27 	  	;then jump to seek/read code.
	ld b,003h		;09a6	06 03 		;How many drives to restore
	ld d,002h		;09a8	16 02 		;Drive select for :1
l09aah:
	ld a,0d0h		;09aa	3e d0 		;Terminate command without interrupt...
	out (0f0h),a		;09ac	d3 f0 		;out to FDC.
	call sub_0a58h		;09ae	cd 58 0a 	;Delay after issuing FDC command
	ld a,d			;09b1	7a 		;
	out (0f4h),a		;09b2	d3 f4 		;Send out drive select value
	rlca			;09b4	07 		;Rotate one bit left, to select next drive
	ld d,a			;09b5	57 		;
	in a,(0f0h)		;09b6	db f0 		;Get FDC status
	and 020h		;09b8	e6 20 		;Is head loaded on drive we selected?
	jr z,l09cah		;09ba	28 0e 		;If it isn't, skip to next drive
	xor a			;09bc	af 		;Restore, no verify, 6 ms step
	out (0f0h),a		;09bd	d3 f0 		;Command out to FDC
	call sub_0a5dh		;09bf	cd 5d 0a 	;Delay after issuing FDC command
l09c2h:
	in a,(0f0h)		;09c2	db f0 		;Get FDC status
	xor 001h		;09c4	ee 01 		;Invert "busy" bit
	and 005h		;09c6	e6 05 		;If not track 0 and command in progress...
	jr z,l09c2h		;09c8	28 f8 		;then keep checking until it's done.
l09cah:
	djnz l09aah		;09ca	10 de 		;Loop again for drives :2 and :3...
	ret			;09cc	c9 		;then return from RST 8.
l09cdh:
	ld a,e			;09cd	7b 		;Cylinder to read
	out (0f3h),a		;09ce	d3 f3 		;Out to FDC data reg (destination of seek)
	ld a,01ch		;09d0	3e 1c 		;Seek -- verify, 6 ms step
	out (0f0h),a		;09d2	d3 f0 		;Out to FDC command reg
	call sub_0a64h		;09d4	cd 64 0a 	;Delay, get FDC status
	ld d,a			;09d7	57 		;Status into D
	and 098h		;09d8	e6 98 		;Get not ready, seek error, CRC error bits
	jr nz,l09e1h		;09da	20 05 	  	;If any of these, don't try to read sector
	ld a,b			;09dc	78 		;Function
	cp 00ch			;09dd	fe 0c 		;
	jr z,l0a00h		;09df	28 1f 		;Jump if it's "read sector"
l09e1h:
	ld b,d			;09e1	42 		;FDC status into B
l09e2h:
	ld d,009h		;09e2	16 09 		;Code for 'Seek Error'
	jr l09eeh		;09e4	18 08 		;Continue with error routine
l09e6h:
	ld d,00bh		;09e6	16 0b 		;'Lost Data'
	bit 2,b			;09e8	cb 50 		;Did we lose any data?
	jr nz,l099fh		;09ea	20 b3 	  	;Jump if yes (error into A, set NZ, RET)
	ld d,00ch		;09ec	16 0c 		;'ID Error'
l09eeh:
	bit 4,b			;09ee	cb 60 		;Record not found?
	jr nz,l099fh		;09f0	20 ad 	  	;Jump if so (Seek:'Seek Err', Read:'ID Err')
	ld d,008h		;09f2	16 08 		;'CRC Error'
	bit 3,b			;09f4	cb 58 		;
	jr nz,l099fh		;09f6	20 a7 	  	;Jump if that's the case
	ld d,004h		;09f8	16 04 		;'Floppy Drive Not Ready'
	bit 7,b			;09fa	cb 78 		;
	jr nz,l099fh		;09fc	20 a1 	  	;Jump if drive wasn't ready
	xor a			;09fe	af 		;Else no errors
	ret			;09ff	c9 		;
l0a00h:
	ld a,(0405ch)		;0a00	3a 5c 40 	;Lowest sector number, 0 or 1
	ld b,a			;0a03	47 		;
	ld d,081h		;0a04	16 81 		;Drive 0, side 0, DDEN
	ld c,086h		;0a06	0e 86 		;Read sector, compare for side 0
	in a,(0f2h)		;0a08	db f2 		;FDC sector register -- sector to read
	cp 012h			;0a0a	fe 12 		;Is sector number less than 18?
	jr c,l0a14h		;0a0c	38 06 		;Jump if it is -- must be SS disk...
	sub 012h		;0a0e	d6 12 		;else it's DS -- subtract 18 from sector no.
	set 4,d			;0a10	cb e2 		;Set drive select for side 1
	set 3,c			;0a12	cb d9 		;Set FDC cmd to compare for side 1
l0a14h:
	add a,b			;0a14	80 		;Add 1 if it's a TRSDOS 1.3 disk
	out (0f2h),a		;0a15	d3 f2 		;Sector number to FDC sector register
	ld a,d			;0a17	7a 		;Drive select value... 
	out (0f4h),a		;0a18	d3 f4 		;out to drive select latch.
	ld b,000h		;0a1a	06 00 		;B will be number of bytes read -- LSB
	ld e,b			;0a1c	58 		;E will be MSB -- set both to 0
	ld a,c			;0a1d	79 		;FDC command into A
	ld c,0f3h		;0a1e	0e f3 		;C points to FDC data register (for INI)
	out (0f0h),a		;0a20	d3 f0 		;Send out FDC command
	ld a,080h		;0a22	3e 80 		;Allow NMI on FDC interrupt request
	out (0e4h),a		;0a24	d3 e4 		;Out to NMI mask
	call sub_0a5dh		;0a26	cd 5d 0a 	;Delay after issuing command to FDC
	ld a,d			;0a29	7a 		;
	out (0f4h),a		;0a2a	d3 f4 		;Send drive select value out again
l0a2ch:
	in a,(0f0h)		;0a2c	db f0 		;FDC status
	bit 1,a			;0a2e	cb 4f 		;Does FDC have any data for us?
	jp nz,l0a39h		;0a30	c2 39 0a 	;Jump if it does
	rlca			;0a33	07 		;Test bit 7 of FDC status
	jp nc,l0a2ch		;0a34	d2 2c 0a 	;If drive is ready, keep waiting for DRQ...
	jr l0a4dh		;0a37	18 14 		;otherwise it's an error -- clean up & return
l0a39h:
	ini			;0a39	ed a2 		;FDC has data...get the first byte
	ld a,d			;0a3b	7a 		;Drive select value
	or 040h			;0a3c	f6 40 		;Set "wait"
l0a3eh:
	out (0f4h),a		;0a3e	d3 f4 		;Out to drive select
	ini			;0a40	ed a2 		;Get byte from FDC (decr B & set Z/NZ)
	jr nz,l0a3eh		;0a42	20 fa 	 	;If not 256 bytes yet, get next one...
	inc e			;0a44	1c 		;else incr MSB of byte counter...
	jp l0a3eh		;0a45	c3 3e 0a 	;and loop until NMI occurs.
l0a48h:
	pop iy			;0a48	fd e1 		;NMI: Discard ret addr (it pts to loop above)
	xor a			;0a4a	af 		;
	out (0e4h),a		;0a4b	d3 e4 		;Don't allow any NMIs
l0a4dh:
	in a,(0f0h)		;0a4d	db f0 		;Get FDC status...
	ld b,a			;0a4f	47 		;into B and C.
	ld c,b			;0a50	48 		;
	call sub_0a56h		;0a51	cd 56 0a 	;Call a RETN and return to next line
	jr l09e6h		;0a54	18 90 		;Set error code, if any, & return from RST 8
sub_0a56h:
	retn			;0a56	ed 45 		;Reset interrupt flip-flops
sub_0a58h:
	push bc			;0a58	c5 		;Delay loop
	ld b,000h		;0a59	06 00 		;The long delay is about 800 usec.
	jr l0a60h		;0a5b	18 03 		;
sub_0a5dh:
	push bc			;0a5d	c5 		;Another delay loop
	ld b,012h		;0a5e	06 12 		;This one is about 60 usec.
l0a60h:
	djnz l0a60h		;0a60	10 fe 		;Repeat this instruction until B is 0...
	pop bc			;0a62	c1 		;then restore BC and return from delay.
	ret			;0a63	c9 		;
sub_0a64h:
	call sub_0a5dh		;0a64	cd 5d 0a 	;Call short delay		
l0a67h:
	in a,(0f0h)		;0a67	db f0 		;Get FDC status
	bit 0,a			;0a69	cb 47 		;
	ret z			;0a6b	c8 		;Return if not busy (done with command)
	bit 7,a			;0a6c	cb 7f 		;
	ret nz			;0a6e	c0 		;Return if not ready
	ld c,a			;0a6f	4f 		;Otherwise save FDC status...
	jr l0a67h		;0a70	18 f5 		;and loop until not busy or not ready.
sub_0a72h:
	ld a,(04067h)		;0a72	3a 67 40 	;Load MODEL%/III file: get disk error...
	or a			;0a75	b7 		;from floppy drive restore command.
	jr nz,l0ad8h		;0a76	20 60 	  	;Jump if there was an error
	ld a,001h		;0a78	3e 01 		;Sector 1...
	ld e,000h		;0a7a	1e 00 		;cylinder 0...
	call sub_0c0bh		;0a7c	cd 0b 0c 	;read it into sector buffer at 4300H.
	jr nz,l0ad8h		;0a7f	20 57 	  	;Jump if any errors
	ld a,e			;0a81	7b 		;Length of sector we just read
	cp 001h			;0a82	fe 01 		;1 = 256 bytes, 2 = 512 bytes
	jr nz,l0a8eh		;0a84	20 08 	  	;If not 256 then jump ('Not Found' error)
	ld a,(04300h)		;0a86	3a 00 43 	;Get first byte of sector
	or a			;0a89	b7 		;Zero on 5.x/6.x disks
	jr z,l0a91h		;0a8a	28 05 		;If zero, then jump, 'cause 5.x/6.x is OK
	cp 0feh			;0a8c	fe fe 		;Is it 0FEH? (found on TRSDOS 1.3 disks)
l0a8eh:
	ld a,000h		;0a8e	3e 00 		;'ROM Image Not Found'  -- If not T1.3...
	ret nz			;0a90	c0 		;then return w/error (strange disk)
l0a91h:
	ld a,c			;0a91	79 		;FDC status from Read Sector call
	and 020h		;0a92	e6 20 		;Get record type (Data Address Mark)
	ld c,001h		;0a94	0e 01 		;Assume TRSDOS 1.3 -- first sector is 1
	ld b,003h		;0a96	06 03 		;Three sectors per granule
	ld hl,(04301h)		;0a98	2a 01 43 	;Get bytes 01, 02 from boot sector
	ld a,l			;0a9b	7d 		;Dir cyl is byte 01 of boot sector
	jr nz,l0aa2h		;0a9c	20 04 	  	;Jump if "deleted" DAM (T1.3 disk) or...
	ld a,h			;0a9e	7c 		;adjust for 5.x/6.x -- dir cyl is byte 02
	dec c			;0a9f	0d 		;Lowest sector number is 0
	ld b,006h		;0aa0	06 06 		;Six sectors per granule
l0aa2h:
	ld (0405dh),a		;0aa2	32 5d 40 	;Store directory cylinder
	ld e,a			;0aa5	5f 		;Directory cylinder value into E
	ld a,b			;0aa6	78 		;Sectors per granule
	ld (04063h),a		;0aa7	32 63 40 	;Store it
	ld a,c			;0aaa	79 		;Lowest sector number
	ld (0405ch),a		;0aab	32 5c 40 	;Store it
	ld a,(0405ch)		;0aae	3a 5c 40 	;Get it again
	or a			;0ab1	b7 		;
	jr nz,l0ac4h		;0ab2	20 10 	  	;Jump if TRSDOS 1.3 disk (always SS)...
	xor a			;0ab4	af 		;otherwise it's a 5.x/6.x disk.
	call sub_0c0bh		;0ab5	cd 0b 0c 	;Read sector 0 of dir cyl (GAT)
	jr nz,l0ad8h		;0ab8	20 1e 	  	;Jump if any errors
	ld a,(043cdh)		;0aba	3a cd 43 	;Get configuration byte (GAT+x'CD')
	and 020h		;0abd	e6 20 		;Number of sides
	ld a,001h		;0abf	3e 01 		;Indicate single-sided
	jr z,l0ac4h		;0ac1	28 01 		;Jump if that's the case...
	inc a			;0ac3	3c 		;else there are two sides.
l0ac4h:
	ld (0405fh),a		;0ac4	32 5f 40 	;Whatever it is, store number of sides
	ld a,002h		;0ac7	3e 02 		;Sector 2 (of directory)
	ld (0405eh),a		;0ac9	32 5e 40 	;Store as next sector to read
l0acch:
	ld hl,(0405dh)		;0acc	2a 5d 40 	;Dir cyl, sector number
	ld e,l			;0acf	5d 		;Cylinder into E
	ld a,h			;0ad0	7c 		;Sector into A
	call sub_0c0bh		;0ad1	cd 0b 0c 	;Read it
	ld a,000h		;0ad4	3e 00 		;'ROM Image Not Found'
	jr z,l0ae0h		;0ad6	28 08 		;Jump if no errors
l0ad8h:
	cp 004h			;0ad8	fe 04 		;Errors while loading ROM image:
	jr nz,l0adfh		;0ada	20 03 	 	;If not 'Floppy Drive Not Ready' then jump
	ld a,00ah		;0adc	3e 0a 		;Else chg to 'Can`t Load Image - Not Ready'
	or a			;0ade	b7 		;Set NZ (unless error 0, 'Image Not Found')
l0adfh:
	ret			;0adf	c9 		;
l0ae0h:
	ld ix,04300h		;0ae0	dd 21 00 43 	;Point to first directory entry in sector
l0ae4h:
	ld a,(ix+000h)		;0ae4	dd 7e 00 	;Get first byte of entry
	bit 7,a			;0ae7	cb 7f 		;Is it an extended dir record?
	jr nz,l0b11h		;0ae9	20 26 	  	;If so, skip it
	bit 4,a			;0aeb	cb 67 		;Is this record in use?
	jr z,l0b11h		;0aed	28 22 		;If not, skip it
	push ix			;0aef	dd e5 		;
	pop hl			;0af1	e1 		;
	ld de,l0005h		;0af2	11 05 00 	;	
	add hl,de		;0af5	19 		;HL points to (DIR+5) -- file name
	ld de,04018h		;0af6	11 18 40 	;Point to 'MODEL%   ' (or MODELA - MODELG)	
	ld b,008h		;0af9	06 08 		;Compare eight bytes of our filename...
	call sub_0bf6h		;0afb	cd f6 0b 	;with filename in directory entry.
	jr nz,l0b11h		;0afe	20 11 	  	;If no match, then skip to next entry
	push ix			;0b00	dd e5 		;Still points to start of entry
	pop hl			;0b02	e1 		;
	ld de,l000bh+2		;0b03	11 0d 00 	;
	add hl,de		;0b06	19 		;HL points to (DIR+13) -- file extension
	ld b,003h		;0b07	06 03 		;Number of bytes to compare
	ld de,l0063h		;0b09	11 63 00 	;Point to 'III'	
	call sub_0bf6h		;0b0c	cd f6 0b 	;Compare them	
	jr z,l0b36h		;0b0f	28 25 		;If they match, we've found it...
l0b11h:
	push ix			;0b11	dd e5 		;otherwise try next directory entry.
	pop hl			;0b13	e1 		;
	ld de,l0020h		;0b14	11 20 00 	;
	add hl,de		;0b17	19 		;HL points to 20H bytes past first entry
	ld a,(0405ch)		;0b18	3a 5c 40 	;Get disk type
	or a			;0b1b	b7 		;
	jr z,l0b22h		;0b1c	28 04 		;Jump if it's 5.x/6.x...
	ld de,l0010h		;0b1e	11 10 00 	;else move pointer 10H bytes more because...
	add hl,de		;0b21	19 		;T1.3 has 30H bytes between dir entries.
l0b22h:
	push hl			;0b22	e5 		;
	pop ix			;0b23	dd e1 		;Points to next dir entry
	ld a,l			;0b25	7d 		;
	or a			;0b26	b7 		;Is pointer at next page of memory?
	jr z,l0b2dh		;0b27	28 04 		;Jump if it is
	cp 0f0h			;0b29	fe f0 		;Is it at byte 0F0H (past last T1.3 entry)?
	jr nz,l0ae4h		;0b2b	20 b7 	  	;Jump if not...
l0b2dh:
	ld a,(0405eh)		;0b2d	3a 5e 40 	;else we've seen all entries in sector.
	inc a			;0b30	3c 		;Incr sector counter and store it, then...
	ld (0405eh),a		;0b31	32 5e 40 	;try next dir sector. (If past end of dir...
	jr l0acch		;0b34	18 96 		;sect not found, get 'Image Not Found' error)
l0b36h:
	ld de,040a2h		;0b36	11 a2 40 	;We found ROM image dir entry -- point to...
	push ix			;0b39	dd e5 		;our buffer for the directory record.
	pop hl			;0b3b	e1 		;HL now points to the directory entry
	ld bc,l0030h		;0b3c	01 30 00 	;Number of bytes to copy
	ldir			;0b3f	ed b0 		;Copy directory record into our buffer
	ld a,0feh		;0b41	3e fe 		;Put 0FEH after directory record, so we...
	ld (de),a		;0b43	12 		;stop there if we're still reading the file.
	ld ix,040b6h		;0b44	dd 21 b6 40 	;Point to first extent field and continue
l0b48h:
	ld e,000h		;0b48	1e 00 		;RST 28H -- Load Object Code: E = byte offset
l0b4ah:
	rst 20h			;0b4a	e7 		;Get byte from file
	ret nz			;0b4b	c0 		;Return if we can't get a byte
	dec a			;0b4c	3d 		;What did we get?
	jr z,l0b5ah		;0b4d	28 0b 		;Jump if it was 01H (load block)
	dec a			;0b4f	3d 		;
	jr z,l0b6bh		;0b50	28 19 		;Jump if it was 02H (transfer address)
	rst 20h			;0b52	e7 		;Else get another byte -- length of block
	ld b,a			;0b53	47 		;Use it as a counter for comment block
l0b54h:
	rst 20h			;0b54	e7 		;Get byte and discard it
	ret nz			;0b55	c0 		;Return if error getting byte
	djnz l0b54h		;0b56	10 fc 		;Loop until we've gotten all of block...
	jr l0b4ah		;0b58	18 f0 		;then get next block in file.
l0b5ah:
	rst 20h			;0b5a	e7 		;Load block -- get block length
	ret nz			;0b5b	c0 		;Return if error
	dec a			;0b5c	3d 		;Decrement by two to allow for load address
	dec a			;0b5d	3d 		;
	ld b,a			;0b5e	47 		;Store number of bytes to load
	call sub_0b6dh		;0b5f	cd 6d 0b 	;Get address for loading into HL
	ret nz			;0b62	c0 		;Return if error
l0b63h:
	rst 20h			;0b63	e7 		;Get byte to load
	ret nz			;0b64	c0 		;Return if error...
	ld (hl),a		;0b65	77 		;else load into memory...
	inc hl			;0b66	23 		;point to next byte in memory...
	djnz l0b63h		;0b67	10 fa 		;and loop until end of block.
	jr l0b4ah		;0b69	18 df 		;Get next block in file
l0b6bh:
	rst 20h			;0b6b	e7 		;Transfer addr -- get block length & discard
	ret nz			;0b6c	c0 		;Return if error
sub_0b6dh:
	rst 20h			;0b6d	e7 		;Get LSB of address
	ret nz			;0b6e	c0 		;
	ld l,a			;0b6f	6f 		;Put it in L
	rst 20h			;0b70	e7 		;Get MSB of address
	ret nz			;0b71	c0 		;
	ld h,a			;0b72	67 		;Put it in H
	ret			;0b73	c9 		;End of RST 28H -- transfer address is in HL
l0b74h:
	push bc			;0b74	c5 		;RST 20H (initially) -- Get Byte from File
	push hl			;0b75	e5 		;Save BC, HL -- used by RST 28H
	ld a,e			;0b76	7b 		;Byte offset in current sector
	or a			;0b77	b7 		;
	jr nz,l0b82h		;0b78	20 08 	  	;If not zero, we don't need to read a sector
	call z,sub_0b88h	;0b7a	cc 88 0b 	;If it IS zero, read the next sector of file
	jr nz,l0b85h		;0b7d	20 06 	  	;Jump if error reading sector...
	ld de,04300h		;0b7f	11 00 43 	;else point to start of sector we just read.
l0b82h:
	xor a			;0b82	af 		;Set Z to indicate success
	ld a,(de)		;0b83	1a 		;Get byte from sector buffer
	inc de			;0b84	13 		;Point to next byte in sector buffer
l0b85h:
	pop hl			;0b85	e1 		;Restore registers, and return
	pop bc			;0b86	c1 		;
	ret			;0b87	c9 		;
sub_0b88h:
	ld hl,04060h		;0b88	21 60 40 	;Point to number of sectors remaining...
	ld a,(hl)		;0b8b	7e 		;in current extent, and get value in A.
	or a			;0b8c	b7 		;
	jr nz,l0b93h		;0b8d	20 04 	  	;Jump if add'l sectors in current extent...
	call sub_0bb7h		;0b8f	cd b7 0b 	;else calculate next extent...
	ret nz			;0b92	c0 		;and return if there's a problem.
l0b93h:
	dec (hl)		;0b93	35 		;Okay...one less unread sector in extent
	ld b,012h		;0b94	06 12 		;One more than highest sector number
	ld a,(0405fh)		;0b96	3a 5f 40 	;Get number of sides on disk
	dec a			;0b99	3d 		;
	jr z,l0b9eh		;0b9a	28 02 		;Jump if SS disk...
	ld b,024h		;0b9c	06 24 		;else new limit for sector number.
l0b9eh:
	ld a,(0405eh)		;0b9e	3a 5e 40 	;Number of last sector we read
	cp b			;0ba1	b8 		;Had we reached end of cylinder?
	jr nz,l0bach		;0ba2	20 08 	  	;If not, skip next four lines...
	ld a,(04062h)		;0ba4	3a 62 40 	;else get cylinder number...
	inc a			;0ba7	3c 		;point to next cylinder...
	ld (04062h),a		;0ba8	32 62 40 	;and store new cylinder number.
	xor a			;0bab	af 		;Next sector read is on new cylinder
l0bach:
	ld c,a			;0bac	4f 		;Last sector number we read
	inc a			;0bad	3c 		;Point to next sector...
	ld (0405eh),a		;0bae	32 5e 40 	;and store it.
	ld a,(04062h)		;0bb1	3a 62 40 	;Get cylinder number...	
	ld e,a			;0bb4	5f 		;and put it in E.
	jr l0c0ch		;0bb5	18 55 		;Jump -- read sector and return from call
sub_0bb7h:
	inc ix			;0bb7	dd 23 		;Find next extent of file -- point to...
	inc ix			;0bb9	dd 23 		;next extent of directory info.
	ld a,(ix+000h)		;0bbb	dd 7e 00 	;Get first byte of extent field
	ld b,a			;0bbe	47 		;
	and 0feh		;0bbf	e6 fe 		;Drop bit 0
	cp 0feh			;0bc1	fe fe 		;Is it 0FEH or 0FFH?
	jr nz,l0bc9h		;0bc3	20 04 	  	;Jump if it isn't...
	ld a,002h		;0bc5	3e 02 		;else it's 'Too Many Extents' error.
	or a			;0bc7	b7 		;Set NZ to indicate error, and return
	ret			;0bc8	c9 		;
l0bc9h:
	ld a,b			;0bc9	78 		;First byte of extent field is starting...
	ld (04062h),a		;0bca	32 62 40 	;cyl of extent -- store as current cyl.
	ld a,(ix+001h)		;0bcd	dd 7e 01 	;Second byte of extent field
	ld e,a			;0bd0	5f 		;Store in E
	and 0e0h		;0bd1	e6 e0 		;Bits 5,6,7 only -- relative gran in cyl
	rlca			;0bd3	07 		;Get value in lowest three bits...
	rlca			;0bd4	07 		;
	rlca			;0bd5	07 		;
	ld b,a			;0bd6	47 		;and store it in B.
	ld a,(04063h)		;0bd7	3a 63 40 	;Get sectors/gran value...
	call sub_0c02h		;0bda	cd 02 0c 	;and multiply to get starting sector.
	ld (0405eh),a		;0bdd	32 5e 40 	;Store as last sector we read
	ld a,e			;0be0	7b 		;Second byte of extent field
	and 01fh		;0be1	e6 1f 		;Get bits 0-4, number of grans in extent
	ld b,a			;0be3	47 		;Store in B
	ld a,(0405ch)		;0be4	3a 5c 40 	;Get flag for disk type
	xor 001h		;0be7	ee 01 		;Change to 0 for DOS 1.x, 1 for 5.x/6.x
	add a,b			;0be9	80 		;Add to # grans in extent, because...
	ld b,a			;0bea	47 		;5.x/6.x uses 0 to indicate 1 gran.
	ld a,(04063h)		;0beb	3a 63 40 	;Get sectors/gran value...
	call sub_0c02h		;0bee	cd 02 0c 	;and multiply by # grans in extent...
	ld (04060h),a		;0bf1	32 60 40 	;to get # sectors in extent, which we store.
	xor a			;0bf4	af 		;Set Z for success (we found next granule)...
	ret			;0bf5	c9 		;and return.
sub_0bf6h:
	ld a,(de)		;0bf6	1a 		;Get character from our filename
	cp 025h			;0bf7	fe 25 		;Is it '%' (wildcard)?
	jr z,l0bfdh		;0bf9	28 02 		;If yes, then anything matches...
	cp (hl)			;0bfb	be 		;otherwise compare with char in dir entry.
	ret nz			;0bfc	c0 		;Return if they don't match...
l0bfdh:
	inc de			;0bfd	13 		;else point to next character of both...
	inc hl			;0bfe	23 		;
	djnz sub_0bf6h		;0bff	10 f5 		;and continue comparing.
l0c01h:
	ret			;0c01	c9 		;Returns with Z set if they match
sub_0c02h:
	ld c,a			;0c02	4f 		;Multiply -- arguments in A & B, result in A
	ld a,b			;0c03	78 		;
	or a			;0c04	b7 		;Multiplying by zero?
	ret z			;0c05	c8 		;If so, we already have the answer
	xor a			;0c06	af 		;Start with zero
l0c07h:
	add a,c			;0c07	81 		;Add original A to it...
	djnz l0c07h		;0c08	10 fd 		;B times.
	ret			;0c0a	c9 		;
sub_0c0bh:
	ld c,a			;0c0b	4f 		;Sector number into C
l0c0ch:
	ld hl,04300h		;0c0c	21 00 43 	;Address of sector buffer in HL
	ld b,00ch		;0c0f	06 0c 		;Function -- read sector
	ld a,001h		;0c11	3e 01 		;Floppy disk
	rst 8			;0c13	cf 		;Do disk I/O...
	ret			;0c14	c9 		;and return.
sub_0c15h:
	ld a,001h		;0c15	3e 01 		;Arcnet boot: This isn't Arcnet boot ROM...
	jp l0151h		;0c17	c3 51 01 	;so it's Error 1 (and halt).
l0c1ah:
	ld a,004h		;0c1a	3e 04 		;RS-232 boot (Network 3) starts here
	ld (04055h),a		;0c1c	32 55 40 	;Use 4 to indicate RS-232 boot
l0c1fh:
	ld a,064h		;0c1f	3e 64 		;Set 8/N/1, assert DTR & RTS
	out (0eah),a		;0c21	d3 ea 		;Out to UART & modem control register
	or a			;0c23	b7 		;Set NZ to get screen address for message
	ld hl,00cd4h		;0c24	21 d4 0c 	;Point to phrases for 'Not Ready '
	rst 18h			;0c27	df 		;Display it
l0c28h:
	in a,(0e8h)		;0c28	db e8 		;Get modem status
	and 020h		;0c2a	e6 20 		;Bit 5 -- Carrier Detect
	jr nz,l0c28h		;0c2c	20 fa 	  	;Loop until we have a carrier
	ld de,l0000h		;0c2e	11 00 00 	;Top left corner of screen
	ld hl,l0cdch		;0c31	21 dc 0c 	;Three spaces, to cover up 'Not'
	rst 10h			;0c34	d7 		;Display them, so screen says '    Ready '
	ld hl,l0ca4h		;0c35	21 a4 0c 	;Point to "Get byte from RS-232" routine
	ld (0400ah),hl		;0c38	22 0a 40 	;Make this address our RST 20H vector
	ld hl,04057h		;0c3b	21 57 40 	;Point to baud rate storage	
l0c3eh:
	ld a,0ffh		;0c3e	3e ff 		;Start at 19.2K bits per second
l0c40h:
	ld (hl),a		;0c40			;Store baud rate
	out (0e9h),a		;0c41	d3 e9 		;Output to baud rate generator
	out (0e8h),a		;0c43	d3 e8 		;Reset UART
	ld b,00ah		;0c45	06 0a 		;Number of tries to receive a byte
l0c47h:
	rst 20h			;0c47	e7 		;Try to receive a byte
	jr nz,l0c4eh		;0c48	20 04 	  	;If unsuccessful, then retry or give up
	cp 055h			;0c4a	fe 55 		;Did we get a 'U'?
	jr z,l0c57h		;0c4c	28 09 		;If yes, then continue...
l0c4eh:
	djnz l0c47h		;0c4e	10 f7 		;else retry up to our limit...
	ld a,(hl)		;0c50	7e 		;then get baud rate...
	sub 011h		;0c51	d6 11 		;and drop down to next lower rate.
	jr nc,l0c40h		;0c53	30 eb 		;If not below slowest rate, then try it...
	jr l0c3eh		;0c55	18 e7 		;else start again at 19.2K bps.
l0c57h:
	ld b,00ah		;0c57	06 0a 		;Number of bytes to receive
l0c59h:
	rst 20h			;0c59	e7 		;Get a byte from RS-232
	jr nz,l0c3eh		;0c5a	20 e2 	  	;If error, start again
	cp 055h			;0c5c	fe 55 		;Did we get a 'U'?
	jr nz,l0c3eh		;0c5e	20 de 	  	;If not, start again...
	djnz l0c59h		;0c60	10 f7 		;else get another byte.
	ld a,(hl)		;0c62	7e 		;Okay, we got ten 'U's...now get the...
	and 00fh		;0c63	e6 0f 		;baud rate we used...
	call sub_0cbeh		;0c65	cd be 0c 	;convert to ASCII & store. (This sets NZ)
	ld hl,l0ce0h		;0c68	21 e0 0c 	;'Found Baud Rate ' + character
	rst 18h			;0c6b	df 		;Display it
	ld hl,l0cc4h		;0c6c	21 c4 0c 	;'Found Baud Rate '	
	call sub_0cb0h		;0c6f	cd b0 0c 	;Transmit message via RS-232
	in a,(0ebh)		;0c72	db eb 		;Clear transmit holding register
l0c74h:
	rst 20h			;0c74	e7 		;Receive a byte
	jr nz,l0c8bh		;0c75	20 14 	  	;If error, go to RS-232 error routine
	cp 0ffh			;0c77	fe ff 		;Did we receive 0FFH?
	jr nz,l0c74h		;0c79	20 f9 	  	;If not, keep trying until we do.
	ld de,l0244h		;0c7b	11 44 02 	;Display at row 9, col 0
	ld hl,l04bch		;0c7e	21 bc 04 	;'Loading ' (v1(15): 'Loading ROM Image')
	push hl			;0c81	e5 		;Save pointer to this message
	rst 10h			;0c82	d7 		;Display it on our screen
	pop hl			;0c83	e1 		;Still points to 'Loading '
	call sub_0cb0h		;0c84	cd b0 0c 	;Send it out RS-232
	rst 28h			;0c87	ef 		;Receive file from RS-232, load into memory
	jr nz,l0c8bh		;0c88	20 01 	  	;If error, go to RS-232 error routine...
	jp (hl)			;0c8a	e9 		;else start executing the file we received.
l0c8bh:
	rrca			;0c8b	0f 		;RS-232 error: Rotate UART status 3 bits...
	rrca			;0c8c	0f 		;to the right -- bit 0 will then indicate...
	rrca			;0c8d	0f 		;overrun, bit 1 framing, bit 2 parity.
	call sub_0cbeh		;0c8e	cd be 0c 	;Convert to ASCII ('A'-'H') & store (sets NZ)
	call sub_0206h		;0c91	cd 06 02 	;Clear the screen (sets BC to 0)
	push bc			;0c94	c5 		;
	ld hl,l0ce8h		;0c95	21 e8 0c 	;'Error ' + character for error encountered
	rst 18h			;0c98	df 		;Display it
	ld hl,00587h		;0c99	21 87 05 	;'Error'
	call sub_0cb0h		;0c9c	cd b0 0c 	;Transmit it
	pop bc			;0c9f	c1 		;Still 0, so scan the keyboard 65536 times...
	rst 30h			;0ca0	f7 		;to let the other system time out and...
	jp l0c1fh		;0ca1	c3 1f 0c 	;re-sync, then we try the RS-232 boot again.
l0ca4h:
	in a,(0eah)		;0ca4	db ea 		;UART & modem control register
	bit 7,a			;0ca6	cb 7f 		;Character received?
	jr z,l0ca4h		;0ca8	28 fa 		;If not, loop until we receive one.
	and 038h		;0caa	e6 38 		;Test for overrun, framing, parity errors
	ret nz			;0cac	c0 		;Return if any errors...
	in a,(0ebh)		;0cad	db eb 		;else get character from UART...
	ret			;0caf	c9 		;and return with Z set to indicate success.
sub_0cb0h:
	in a,(0eah)		;0cb0	db ea 		;UART & modem control register
	and 040h		;0cb2	e6 40 		;Transmit register empty?
	jr z,sub_0cb0h		;0cb4	28 fa 		;If not, loop until it is.
	ld a,(hl)		;0cb6	7e 		;Get character to transmit
	or a			;0cb7	b7 		;Is it zero?
	ret z			;0cb8	c8 		;If zero, we're done transmitting...
	out (0ebh),a		;0cb9	d3 eb 		;else send character to UART xmit register...
	inc hl			;0cbb	23 		;point to next character...
	jr sub_0cb0h		;0cbc	18 f2 		;and repeat.
sub_0cbeh:
	add a,041h		;0cbe	c6 41 		;Convert value 0-15 to ASCII...
	ld (04068h),a		;0cc0	32 68 40 	;store it...
	ret			;0cc3	c9 		;and return.
l0cc4h:
    defb    'Found Baud Rate ', 0
l0cd5h:
	nop			;0cd5	00 		
	call nc,05004h		;0cd6	d4 04 50 	
	dec b			;0cd9	05 		
	rst 38h			;0cda	ff 		
	rst 38h			;0cdb	ff 		
l0cdch:
	jr nz,l0cfeh		;0cdc	20 20 	    
	jr nz,l0ce0h		;0cde	20 00 	  
l0ce0h:
	add a,h			;0ce0	84 	
	ld bc,l0cc4h		;0ce1	01 c4 0c 	
	ld l,b			;0ce4	68 		
	ld b,b			;0ce5	40 		
	rst 38h			;0ce6	ff 		
	rst 38h			;0ce7	ff 		
l0ce8h:
	call nz,08703h		;0ce8	c4 03 87 	
	dec b			;0ceb	05 		
	adc a,l			;0cec	8d 		
	dec b			;0ced	05 		
	ld l,b			;0cee	68 		
	ld b,b			;0cef	40 		
	rst 38h			;0cf0	ff 		
	rst 38h			;0cf1	ff 		
l0cf2h:
ramTest:
	ld sp,03ffeh		;0cf2	31 fe 3f 	;RAM Test: Put stack in video memory
	ld iy,03c80h		;0cf5	fd 21 80 3c 	;Point to row 2, col 0 of screen
	ld a,008h		;0cf9	3e 08 		;2 MHz clock, disable I/O bus,...
	out (0ech),a		;0cfb	d3 ec 		;enable alt char set.
	xor a			;0cfd	af 		;Set 0000H-37FFH as read-only,...
l0cfeh:
	out (084h),a		;0cfe	d3 84 		;Model III memory map.
	ld ix,l0d14h		;0d00	dd 21 14 0d 	;Address to go to after clearing screen.
l0d04h:
	ld a,020h		;0d04	3e 20 		;The routine starting here...
	ld de,03c00h		;0d06	11 00 3c 	;is called from...
	ld (de),a		;0d09	12 		;several locations...
	ld h,d			;0d0a	62 		;in the RAM test.
	ld l,e			;0d0b	6b 		;It clears the screen...
	ld bc,l03ffh		;0d0c	01 ff 03 	;and then...
	inc de			;0d0f	13 		;jumps to the...
	ldir			;0d10	ed b0 		;address contained in...
	jp (ix)			;0d12	dd e9 		;the IX register.
l0d14h:
	ld hl,l0ed2h		;0d14	21 d2 0e 	;'Dynamic RAM Test'
	ld de,03c00h		;0d17	11 00 3c 	;Top of screen
	call l0249h		;0d1a	cd 49 02 	;Display it
	ld hl,l0ee3h		;0d1d	21 e3 0e 	;'Press <ENTER> To Begin...'
	ld de,03c40h		;0d20	11 40 3c 	;Next row on screen
	call l0249h		;0d23	cd 49 02 	;Display it
	ld hl,l0f0ch		;0d26	21 0c 0f 	;'Errors will be displayed'
	ld de,l00c0h		;0d29	11 c0 00 	;Next row on screen
	call l0249h		;0d2c	cd 49 02 	;Display it
l0d2fh:
	ld a,(03840h)		;0d2f	3a 40 38 	;Column 6 of keyboard matrix
	cp 001h			;0d32	fe 01 		;Is <ENTER> pressed?
	jr nz,l0d2fh		;0d34	20 f9 	  	;Loop until it is.
	ld ix,l0d3dh		;0d36	dd 21 3d 0d 	;Address of next part of test
	jp l0d04h		;0d3a	c3 04 0d 	;Clear screen, then continue.
l0d3dh:
	ld hl,l0f32h		;0d3d	21 32 0f 	;Column headings for display
	ld de,03c80h		;0d40	11 80 3c 	;Row 2, col 0 of screen
	call l0249h		;0d43	cd 49 02 	;Display it
	ld hl,l0f92h		;0d46	21 92 0f 	;'Stack'
	ld de,l03f4h		;0d49	11 f4 03 	;Row 15, col 52 of screen
	call l0249h		;0d4c	cd 49 02 	;Display it
l0d4fh:
	ld hl,l0f56h		;0d4f	21 56 0f 	;'Test Pattern is 55h'
	ld de,03c40h		;0d52	11 40 3c 	;Row 1, col 0 of screen
	call l0249h		;0d55	cd 49 02 	;Display it
	ld hl,04000h		;0d58	21 00 40 	;Fill memory from 4000H...
	ld d,h			;0d5b	54 		;to 0FFFFH...
	ld e,l			;0d5c	5d 		;with 55H (01010101 binary).
	ld bc,0bfffh		;0d5d	01 ff bf 	;
	ld a,055h		;0d60	3e 55 		;
	ld (de),a		;0d62	12 		;
	inc de			;0d63	13 		;
	ldir			;0d64	ed b0 		;
	ld hl,04000h		;0d66	21 00 40 	;Start checking memory at 4000H
	ld bc,0bfffh		;0d69	01 ff bf 	;Number of bytes to check
l0d6ch:
	ld a,055h		;0d6c	3e 55 		;Compare 55H...
	ld e,(hl)		;0d6e	5e 		;with the value we find in memory.
	cp e			;0d6f	bb 		;If different, something's wrong with memory.
	ld ix,l0d77h		;0d70	dd 21 77 0d 	;Where to resume if we jump
	jp nz,l0e0ah		;0d74	c2 0a 0e 	;If memory error, then display it
l0d77h:
	dec bc			;0d77	0b 		;One less byte left to check
	inc hl			;0d78	23 		;Point to next byte to check
	ld a,c			;0d79	79 		;Have we checked all the bytes?
	or b			;0d7a	b0 		;
	jr nz,l0d6ch		;0d7b	20 ef 	  	;If not, then check the next one
	ld hl,l0f6ah		;0d7d	21 6a 0f 	;Else point to 'AAh'
	ld de,03c50h		;0d80	11 50 3c 	;Row 1, col 16 of screen
	call l0249h		;0d83	cd 49 02 	;Display it, so we see 'Test Pattern is AAh'
	ld hl,04000h		;0d86	21 00 40 	;The same test again, except...
	ld d,h			;0d89	54 		;this time...
	ld e,l			;0d8a	5d 		;we fill...
	ld bc,0bfffh		;0d8b	01 ff bf 	;4000H to 0FFFFH...
	ld a,0aah		;0d8e	3e aa 		;with 0AAH (10101010 binary).
	ld (de),a		;0d90	12 		;
	inc de			;0d91	13 		;
	ldir			;0d92	ed b0 		;
	ld hl,04000h		;0d94	21 00 40 	;
	ld bc,0bfffh		;0d97	01 ff bf 	;Compare contents of memory...
l0d9ah:
	ld a,0aah		;0d9a	3e aa 		;with 0AAH this time.
	ld e,(hl)		;0d9c	5e 		;
	cp e			;0d9d	bb 		;
	ld ix,l0da5h		;0d9e	dd 21 a5 0d 	;
	jp nz,l0e0ah		;0da2	c2 0a 0e 	;Jump if memory error, resume at next line
l0da5h:
	dec bc			;0da5	0b 		;
	inc hl			;0da6	23 		;
	ld a,c			;0da7	79 		;
	or b			;0da8	b0 		;
	jp nz,l0d9ah		;0da9	c2 9a 0d 	;Loop until we've tested all the bytes.
	ld d,000h		;0dac	16 00 		;Mask for testing each byte
l0daeh:
	ld b,d			;0dae	42 		;Save mask in B for a moment
	ld hl,l0f6eh		;0daf	21 6e 0f 	;'Modified Address Test  ld (hl),mask'	
	ld de,03c40h		;0db2	11 40 3c 	;Second line of screen	
	call l0249h		;0db5	cd 49 02 	;Display it (overwrite 'Test Pattern...')
	ld h,d			;0db8	62 		;Address of last character displayed...
	ld l,e			;0db9	6b 		;into HL.
	ld d,b			;0dba	50 		;Mask back into D
	inc hl			;0dbb	23 		;Point to next space on screen.
	rra			;0dbc	1f 		;There should be a LD A,D before this...
	rra			;0dbd	1f 		;to get the mask value into A.  See text.
	rra			;0dbe	1f 		;Anyway, move the upper nybble of the...
	rra			;0dbf	1f 		;"mask" value into the lower nybble.
	and 00fh		;0dc0	e6 0f 		;Get lower (formerly upper) 4 bits only
	add a,090h		;0dc2	c6 90 		;Convert to one ASCII character...
	daa			;0dc4	27 		;using the usual algorithm...
	adc a,040h		;0dc5	ce 40 		;so we can display the top half...
	daa			;0dc7	27 		;of the "mask" value.
	ld (hl),a		;0dc8	77 		;Put character on screen.
	inc hl			;0dc9	23 		;Point to next space on screen.
	ld a,d			;0dca	7a 		;Get mask value
	and 00fh		;0dcb	e6 0f 		;Lower nybble only
	add a,090h		;0dcd	c6 90 		;Convert to one ASCII character again
	daa			;0dcf	27 		;
	adc a,040h		;0dd0	ce 40 		;
	daa			;0dd2	27 		;
	ld (hl),a		;0dd3	77 		;Display 2nd char of mask value
	ld hl,04000h		;0dd4	21 00 40 	;Start testing at 4000H
	ld bc,0bfffh		;0dd7	01 ff bf 	;Number of bytes to test
l0ddah:
	ld a,d			;0dda	7a 		;Get mask value...
	xor l			;0ddb	ad 		;XOR with LSB of address...
	xor h			;0ddc	ac 		;and with MSB of address...
	ld (hl),a		;0ddd	77 		;and put it into memory.
	inc hl			;0dde	23 		;Loop until...
	dec bc			;0ddf	0b 		;
	ld a,c			;0de0	79 		;
	or b			;0de1	b0 		;
	jr nz,l0ddah		;0de2	20 f6 		;we've tested all the bytes.
	ld a,d			;0de4	7a 		;Mask value
	and 003h		;0de5	e6 03 		;Is mask divisible by four?
	jr nz,l0dedh		;0de7	20 04 		;If not, skip next two lines...
	ld a,040h		;0de9	3e 40 		;else run the drive motors a bit to put...
	out (0f4h),a		;0deb	d3 f4 		;add'l load on power supply, I suppose.
l0dedh:
	ld hl,04000h		;0ded	21 00 40 	;Start testing at 4000H again
	ld bc,0bfffh		;0df0	01 ff bf 	;number of bytes to test
l0df3h:
	ld a,d			;0df3	7a 		;Mask...
	xor l			;0df4	ad 		;XOR lsb of address...	
	xor h			;0df5	ac 		;and msb of address...
	ld e,(hl)		;0df6	5e 		;should match what we put there...
	cp e			;0df7	bb 		;a moment ago.
	ld ix,l0dfeh		;0df8	dd 21 fe 0d 	;Where to resume if error
	jr nz,l0e0ah		;0dfc	20 0c 	  	;If memory error, display it & resume
l0dfeh:
	inc hl			;0dfe	23 		;Loop until we've...
	dec bc			;0dff	0b 		;
	ld a,c			;0e00	79 		;
	or b			;0e01	b0 		;
	jr nz,l0df3h		;0e02	20 ef 	  	;tested all the bytes...
	inc d			;0e04	14 		;then increment mask...
	jr nz,l0daeh		;0e05	20 a7 	  	;and test again, unless mask was up to 0FFH.
	jp l0d4fh		;0e07	c3 4f 0d 	;If it was, repeat entire test, endlessly.
l0e0ah:
	ex af,af'		;0e0a	08 		;Memory Error display routine:
	exx			;0e0b	d9 		;Use alternate AF, BC, DE, HL
	ld bc,00040h		;0e0c	01 40 00 	;Number of characters on one line of screen
	add iy,bc		;0e0f	fd 09 		;Point to next line on screen
	ld bc,0c040h		;0e11	01 40 c0 	;(0C040H = negative 3FC0H)
	add iy,bc		;0e14	fd 09 		;Did IY point to the last line...
	jr nc,l0e1ch		;0e16	30 04 	0 	;of the screen?  Jump if it didn't...
	ld iy,l0000h		;0e18	fd 21 00 00 	;else keep it at the bottom line.
l0e1ch:
	ld bc,03fc0h		;0e1c	01 c0 3f 	;Cancels out addition of 0C040H
	add iy,bc		;0e1f	fd 09 		;
	exx			;0e21	d9 		;Restore original BC,DE,HL
	ld a,h			;0e22	7c 		;High byte of address of defective byte
	rra			;0e23	1f 		;Move upper four bits down
	rra			;0e24	1f 	. 
	rra			;0e25	1f 	. 
	rra			;0e26	1f 	. 
	and 00fh		;0e27	e6 0f 		;Use upper (now lower) 4 bits only
	add a,090h		;0e29	c6 90 		;Convert to one ASCII character
	daa			;0e2b	27 	' 
	adc a,040h		;0e2c	ce 40 	. @ 
	daa			;0e2e	27 		;Display character under 'Address' heading...	
	ld (iy+001h),a		;0e2f	fd 77 01 	;at col 1 of current row.
	ld a,h			;0e32	7c 		;Repeat for high byte of address...
	and 00fh		;0e33	e6 0f 		;lower 4 bits this time.
	add a,090h		;0e35	c6 90 		;Convert to ASCII
	daa			;0e37	27 	' 
	adc a,040h		;0e38	ce 40 	. @ 
	daa			;0e3a	27 	' 
	ld (iy+002h),a		;0e3b	fd 77 02 	;Display at col 2 of current line
	ld a,l			;0e3e	7d 		;Repeat for low byte of address
	rra			;0e3f	1f 	. 
	rra			;0e40	1f 	. 
	rra			;0e41	1f 	. 
	rra			;0e42	1f 	. 
	and 00fh		;0e43	e6 0f 	. . 
	add a,090h		;0e45	c6 90 	. . 
	daa			;0e47	27 	' 
	adc a,040h		;0e48	ce 40 	. @ 
	daa			;0e4a	27 	' 
	ld (iy+003h),a		;0e4b	fd 77 03 	. w . 
	ld a,l			;0e4e	7d 	} 
	and 00fh		;0e4f	e6 0f 	. . 
	add a,090h		;0e51	c6 90 	. . 
	daa			;0e53	27 	' 
	adc a,040h		;0e54	ce 40 	. @ 
	daa			;0e56	27 	' 
	ld (iy+004h),a		;0e57	fd 77 04 	;Display in cols 3 & 4 of current line
	ld a,(hl)		;0e5a	7e 		;Read contents of problem address again
	ld i,a			;0e5b	ed 47 		;Use I register as temporary storage
	rra			;0e5d	1f 		;Convert it to ASCII
	rra			;0e5e	1f 	. 
	rra			;0e5f	1f 	. 
	rra			;0e60	1f 	. 
	and 00fh		;0e61	e6 0f 	. . 
	add a,090h		;0e63	c6 90 	. . 
	daa			;0e65	27 	' 
	adc a,040h		;0e66	ce 40 	. @ 
	daa			;0e68	27 	' 
	ld (iy+00ah),a		;0e69	fd 77 0a 	. w . 
	ld a,i		;0e6c	ed 57 	. W 
	and 00fh		;0e6e	e6 0f 	. . 
	add a,090h		;0e70	c6 90 	. . 
	daa			;0e72	27 	' 
	adc a,040h		;0e73	ce 40 	. @ 
	daa			;0e75	27 		;Display under 'Retry' heading...	
	ld (iy+00bh),a		;0e76	fd 77 0b 	;in cols 10,11 of current line.
	ld a,e			;0e79	7b 		;Value originally read from problem address
	rra			;0e7a	1f 		;Convert to ASCII, etc.
	rra			;0e7b	1f 		
	rra			;0e7c	1f 		
	rra			;0e7d	1f 		
	and 00fh		;0e7e	e6 0f 	. . 
	add a,090h		;0e80	c6 90 	. . 
	daa			;0e82	27 	' 
	adc a,040h		;0e83	ce 40 	. @ 
	daa			;0e85	27 	' 
	ld (iy+011h),a		;0e86	fd 77 11 	. w . 
	ld a,e			;0e89	7b 	{ 
	and 00fh		;0e8a	e6 0f 	. . 
	add a,090h		;0e8c	c6 90 	. . 
	daa			;0e8e	27 	' 
	adc a,040h		;0e8f	ce 40 	. @ 
	daa			;0e91	27 	' 
	ld (iy+012h),a		;0e92	fd 77 12 	;Display under 'Read' heading, cols 17,18
	ex af,af'		;0e95	08 		;Restore original AF register...
	ld i,a			;0e96	ed 47 		;save it in I for a moment...
	rra			;0e98	1f 		;this is the value we expected to find...
	rra			;0e99	1f 		;at the problem address, and didn't.
	rra			;0e9a	1f 		;Convert to ASCII, etc.
	rra			;0e9b	1f 	. 
	and 00fh		;0e9c	e6 0f 	. . 
	add a,090h		;0e9e	c6 90 	. . 
	daa			;0ea0	27 	' 
	adc a,040h		;0ea1	ce 40 	. @ 
	daa			;0ea3	27 	' 
	ld (iy+017h),a		;0ea4	fd 77 17 	. w . 
	ld a,i		;0ea7	ed 57 	. W 
	and 00fh		;0ea9	e6 0f 	. . 
	add a,090h		;0eab	c6 90 	. . 
	daa			;0ead	27 	' 
	adc a,040h		;0eae	ce 40 	. @ 
	daa			;0eb0	27 	' 
	ld (iy+018h),a		;0eb1	fd 77 18 	. w . 
	ld a,d			;0eb4	7a 	z 
	rra			;0eb5	1f 	. 
	rra			;0eb6	1f 	. 
	rra			;0eb7	1f 	. 
	rra			;0eb8	1f 	. 
	and 00fh		;0eb9	e6 0f 	. . 
	add a,090h		;0ebb	c6 90 	. . 
	daa			;0ebd	27 	' 
	adc a,040h		;0ebe	ce 40 	. @ 
	daa			;0ec0	27 	' 
	ld (iy+020h),a		;0ec1	fd 77 20 	. w   
	ld a,d			;0ec4	7a 	z 
	and 00fh		;0ec5	e6 0f 	. . 
	add a,090h		;0ec7	c6 90 	. . 
	daa			;0ec9	27 	' 
	adc a,040h		;0eca	ce 40 	. @ 
	daa			;0ecc	27 	' 
	ld (iy+021h),a		;0ecd	fd 77 21 	;Display under 'Mask' heading, cols 32,33
	jp (ix)			;0ed0	dd e9 		;Resume test at address in IX
l0ed2h:
    defb    'Dynamic RAM Test', 0	;Zero byte indicates end of message
l0ee3h:
    defb    'Press <Enter> To Begin And RESET To Exit', 0
l0f0ch:
    defb    'Any Errors Detected Will Be Displayed', 0
l0f32h:
    defb    'Address  Retry  Read  Written  Mask', 0
l0f56h:
    defb    'Test Pattern Is 55h', 0
l0f6ah:
    defb    'AAh', 0
l0f6eh:
    defb    'Modified Address Test  ld (hl),mask', 0
l0f92h:
    defb    'Stack', 0
	nop			;0f98	00 	. 
	nop			;0f99	00 	. 
	nop			;0f9a	00 	. 
	nop			;0f9b	00 	. 
	nop			;0f9c	00 	. 
	nop			;0f9d	00 	. 
	nop			;0f9e	00 	. 
	nop			;0f9f	00 	. 
	nop			;0fa0	00 	. 
	nop			;0fa1	00 	. 
	nop			;0fa2	00 	. 
	nop			;0fa3	00 	. 
	nop			;0fa4	00 	. 
	nop			;0fa5	00 	. 
	nop			;0fa6	00 	. 
	nop			;0fa7	00 	. 
	nop			;0fa8	00 	. 
	nop			;0fa9	00 	. 
	nop			;0faa	00 	. 
	nop			;0fab	00 	. 
	nop			;0fac	00 	. 
	nop			;0fad	00 	. 
	nop			;0fae	00 	. 
	nop			;0faf	00 	. 
	nop			;0fb0	00 	. 
	nop			;0fb1	00 	. 
	nop			;0fb2	00 	. 
	nop			;0fb3	00 	. 
	nop			;0fb4	00 	. 
	nop			;0fb5	00 	. 
	nop			;0fb6	00 	. 
	nop			;0fb7	00 	. 
	nop			;0fb8	00 	. 
	nop			;0fb9	00 	. 
	nop			;0fba	00 	. 
	nop			;0fbb	00 	. 
	nop			;0fbc	00 	. 
	nop			;0fbd	00 	. 
	nop			;0fbe	00 	. 
	nop			;0fbf	00 	. 
	nop			;0fc0	00 	. 
	nop			;0fc1	00 	. 
	nop			;0fc2	00 	. 
	nop			;0fc3	00 	. 
	nop			;0fc4	00 	. 
	nop			;0fc5	00 	. 
	nop			;0fc6	00 	. 
	nop			;0fc7	00 	. 
	nop			;0fc8	00 	. 
	nop			;0fc9	00 	. 
	nop			;0fca	00 	. 
	nop			;0fcb	00 	. 
	nop			;0fcc	00 	. 
	nop			;0fcd	00 	. 
	nop			;0fce	00 	. 
	nop			;0fcf	00 	. 
	nop			;0fd0	00 	. 
	nop			;0fd1	00 	. 
	nop			;0fd2	00 	. 
	nop			;0fd3	00 	. 
	nop			;0fd4	00 	. 
	nop			;0fd5	00 	. 
	nop			;0fd6	00 	. 
	nop			;0fd7	00 	. 
	nop			;0fd8	00 	. 
	nop			;0fd9	00 	. 
	nop			;0fda	00 	. 
	nop			;0fdb	00 	. 
	nop			;0fdc	00 	. 
	nop			;0fdd	00 	. 
	nop			;0fde	00 	. 
	nop			;0fdf	00 	. 
	nop			;0fe0	00 	. 
	nop			;0fe1	00 	. 
	nop			;0fe2	00 	. 
	nop			;0fe3	00 	. 
	nop			;0fe4	00 	. 
	nop			;0fe5	00 	. 
	nop			;0fe6	00 	. 
	nop			;0fe7	00 	. 
	nop			;0fe8	00 	. 
	nop			;0fe9	00 	. 
	nop			;0fea	00 	. 
    defb    '(c) 1983, Tandy Corp.'

	end
